import os
import importlib
import argparse

def main():
    # Define command line arguments parser
    parser = argparse.ArgumentParser()
    parser.add_argument('--station_code', type=str, help='Station to predict snow/no-snow flag for.', required=True)
    parser.add_argument('--year', type=str, help='Year to predict snow/no-snow flag for.', required=True)
    parser.add_argument('--data_path', type=str, help='Path to the folder with CSV data.', required=True)
    parser.add_argument('--metadata_path', type=str, help='Path to the IMIS station metadata file.', required=True)
    parser.add_argument('--module_name', type=str, help='Path to the snowflag model module to import.', required=True)
    parser.add_argument('--model_file', type=str, help='Path to the ONNX model file to be used.', required=True)
    parser.add_argument('--model_batch_size', type=int, default=64, help='How many samples should be fed to the model in a single batch. Defalts to 64.')
    args = parser.parse_args()

    # Check validity of some command line arguments
    assert args.model_batch_size >= 8, "Model batch size has to be >= 8."

    # Produce snow/no-snow classificaiton for all years for all stations in the list of stations
    print('Processing station %s, year %s ...' % (args.station_code, args.year))
    
    if len(args.year) > 0:
        data_subsets = [[args.station_code, 'measure_date.dt.year == %s' % (args.year), True]]
    else:
        data_subsets = [[args.station_code, '', True]]
        
    snonosnow_module = importlib.import_module(args.module_name)
    model = snonosnow_module.SnowNoSnowModel(args.model_file)
    model_name = os.path.splitext(os.path.split(args.model_file)[-1])[0]

    model.load_dataset(
        data_subsets=data_subsets, 
        data_path=args.data_path,
        metadata_path=args.metadata_path,
        batch_size=args.model_batch_size,
    )
    if len(model.dataset) > 0:
        classif, tstamps = model.run()
        df = model.dataset.df
        df['measure_date'] = df['measure_date'].dt.tz_localize(None)
        df = df.set_index('measure_date')
        df = df.loc[tstamps]
        df = df.reset_index()
        df = df.rename(columns={'level_0': 'measure_date'})
        df['snowflag'] = classif.tolist()
        if len(args.year) == 0:
            year = 'all'
        else:
            year = args.year
        df.to_csv('snowflag_%s_%s_%s.csv' % (model_name, args.station_code, year))

    print('Done.')

if __name__ == "__main__":
    main()
