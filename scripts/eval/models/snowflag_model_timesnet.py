import sys
sys.path.append('../../')

import pandas as pd
from ml.data.dataset import StationSequenceDataset
from torch.utils.data import DataLoader

from typing import List, Any

import torch
import onnxruntime

class SnowNoSnowModel(object):
    def __init__(self, model_file : str) -> None:
        # Load the model
        self.load_model(model_file)
    
    def load_dataset(self, data_subsets : List[Any], data_path : str, metadata_path : str, batch_size : int = 64):
        # Initialize dataset class with configuration corresponding to the chosen model
        self.dataset = StationSequenceDataset(data_subsets=data_subsets,
                                  input_feats=self.input_feats,
                                  target_feats=['no_snow'],
                                  seq_len=self.seq_len,
                                  max_dist_timesteps=self.seq_len,
                                  sampling_filter='',
                                  normalization=('none', 'none'), # Normalization is part of the ONNX model wrapper
                                  norm_inputs=None,
                                  norm_targets=None,
                                  return_timestamp=True,
                                  return_time_encoding=True,
                                  data_path=data_path,
                                  metadata_path=metadata_path)
        # ... and a dataloader for batching
        self.dataloader = DataLoader(self.dataset, batch_size=batch_size, shuffle=False, drop_last=False)

    def load_model(self, model_file : str):
        # Load configuration file which stores model configuration
        self.onnxrt_session = onnxruntime.InferenceSession(model_file)  

        self.input_feats = self.onnxrt_session.get_modelmeta().custom_metadata_map['input_features'].split(';')
        self.seq_len = int(self.onnxrt_session.get_modelmeta().custom_metadata_map['seq_len'])

    def run(self):
        clas = []
        tstamps = []
        # Classify snow/no-snow for all samples in the dataset
        for i, data in enumerate(self.dataloader):
            with torch.no_grad():
                inputs, tenc, _, tstamp = data
                cl = self.onnxrt_session.run(None, {'input': inputs.cpu().numpy(), 'time_enc': tenc.cpu().numpy()})

            clas.append(torch.tensor(cl[0]))
            tstamps.append(tstamp)

        clas = torch.cat(clas).squeeze().float().cpu().numpy().copy()#[:, 0]
        tstamps = torch.cat(tstamps)

        #clas = np.concatenate([np.ones((self.predict_timestep+1, )) * clas[0], clas], axis=0)

        datetimes = pd.to_datetime(tstamps[:, -1] * 10**9)

        return clas, datetimes
    
