import sys
sys.path.append('../../')

import pandas as pd

from ml.data.dataset import StationSequenceDataset
from torch.utils.data import DataLoader

from typing import List, Any

import torch
import pickle

class SnowNoSnowModel(object):
    def __init__(self, model_file : str) -> None:
        # Load the model
        self.load_model(model_file)
    
    def load_dataset(self, data_subsets : List[Any], data_path : str, metadata_path : str, batch_size : int = 64):
        # Initialize dataset class with configuration corresponding to the chosen model
        self.dataset = StationSequenceDataset(data_subsets=data_subsets,
                                  input_feats=self.input_feats,
                                  target_feats=['no_snow'],
                                  seq_len=self.seq_len,
                                  max_dist_timesteps=self.seq_len,
                                  sampling_filter='',
                                  normalization=('meanstd', 'none'),
                                  norm_inputs=([-1.1219, 85.6884, 70.0613, 1.8792, 84.2651, 1.9107, 0.2241], [ 10.3191, 156.2701, 26.0700, 9.2310, 96.8068, 2.1099, 32.8813]), # Unfortunately this is not stored in the model file for Random Forest models
                                  norm_targets=None,
                                  return_timestamp=True,
                                  data_path=data_path,
                                  metadata_path=metadata_path)
        # ... and a dataloader for batching
        self.dataloader = DataLoader(self.dataset, batch_size=batch_size, shuffle=False, drop_last=False)

    def load_model(self, model_file : str):
        # Load configuration file which stores model configurationimport pickle 
        self.model = pickle.load(open(model_file, 'rb'))
        
        self.input_feats = ['TSS_30MIN_MEAN', 'RSWR_30MIN_MEAN', 'RH_30MIN_MEAN', 'TA_30MIN_MEAN', 'HS', 'VW_30MIN_MEAN', 'solar_altitude']
        self.seq_len = 48
        self.predict_timestep = 47

    def run(self):

        inp_data = []
        inp_tstamps = []
        # Classify snow/no-snow for all samples in the dataset
        for i, data in enumerate(self.dataloader):
            inp_data.append(data[0])
            inp_tstamps.append(data[2])
        inp_data = torch.cat(inp_data, dim=0)
        inp_tstamps = torch.cat(inp_tstamps, dim=0)

        clas = self.model.predict_proba(inp_data[:, self.predict_timestep, :].cpu().numpy())[:, 0]
        datetimes = pd.to_datetime(inp_tstamps[:, self.predict_timestep] * 10**9)

        return clas, datetimes
    
