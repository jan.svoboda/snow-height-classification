# Snow Height Classification 

This is a repository for publication "Towards deep learning solutions for classification of automated snow height measurements".

It allows to reproduce all results presented in the paper and use the code for further research in the field.

## Dependencies

Dependencies needed to run this repository are listed in the `env.yaml` file, which can be also used to create corresponding conda environment:

`conda env create -f env.yml`

Afterwards, you can activate the environment by running:

`conda activate hs-classif`

## Data

All data needed to run scripts in this repository are published at:
[https://doi.org/10.5281/zenodo.13324736](https://doi.org/10.5281/zenodo.13324736)

## Training
All models that we have experimented with in the paper are available to be trained using this repository. 

### Neural network models
All neural network models presented in this study can be trained via PyTorch Lightning CLI, an example follows:

`python main.py fit -c configs/models/4_feats/tcn_classif_annot_noint_classbal_aw_nosalt_novw_norh_paper.yaml`

### Random forests
Random forest models compared in this work have been trained using Jupyter Notebooks which can be found in:

`notebooks/random_forest/`

## Converting model checkpoints to ONNX
Each trained model can be converted into ONNX file using Jupter Notebooks provided in:

`notebooks/models_to_onnx/`

An example model checkpoints to test the conversion are in:

`resources/checkpoints/`

All versions of models used in our study in ONNX format are provided in:

`resources/onnx_models/`

## Evaluation
Models have been evaluated through ONNX runtime with the exception of Random Forests, which have been stored using Pickle.

### Generating predictions 
Predictions for each model used in this study can be generated using

`scripts/eval/snowflag_eval.py`

An example would be:

`python snowflag_eval.py --station_code WFJ2 --year '' --data_path='../../snow-height-classification-dataset/snow-nosnow-dataset/' --metadata_path='../../snow-height-classification-dataset/station-metadata/network-map.kml' --module_name snowflag_model --model_file '../../resources/onnx_models/models/4_feats/tcn_classif_annot_classbal_aw_nosalt_novw_norh_paper_new.onnx'` 

### Visualizing results

All plots available in the paper can be reproduced using Jupyter Notebooks located in:

`notebooks/experiments/`

In order to ease eventual exploration of the results, we provide predictions for the evaluated models as part of the dataset mentioned [above](#data).

# List of authors

* Jan Svoboda ([jan.svoboda@slf.ch](mailto:jan.svoboda@slf.ch))
* Marc Ruesch (WSL Institute for Snow and Avalanche Research SLF)
* David Liechti (WSL Institute for Snow and Avalanche Research SLF)
* Corinne Jones (Swiss Data Science Center)
* Michele Volpi (Swiss Data Science Center)
* Michael Zehnder (WSL Institute for Snow and Avalanche Research SLF)
* Jürg Schweizer (WSL Institute for Snow and Avalanche Research SLF)

# Acknowledgements
This work has been conducted with the support of fundings from:
* Swiss Data Science Center (Grant/Award: C21-15L)
* WSL Institute for Snow and Avalanche Research SLF

# License
Code in this repository is available for research under the CC BY-NC license:
[https://creativecommons.org/licenses/by-nc/4.0/](https://creativecommons.org/licenses/by-nc/4.0/)