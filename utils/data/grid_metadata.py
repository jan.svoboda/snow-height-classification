import os
from typing import List, Dict
from pykml import parser


class GridMetadata(object):
    """
    Class to store metadata of a selected network grid. Currently 'IMIS' and 'SMN' networks are supported.
    This class stores a dictionary of the following form:
    {
        'station_id': [latitude, longitude, elevation]
    }

    Parameters
    ----------    
        network_id : str
            ID of the network to load metadata for. One of ['IMIS', 'SMN']

        data_path : str
            Path to the data files.
    """

    def __init__(self, network_id : str, data_path : str = '') -> None:
        super(GridMetadata, self).__init__()

        metadata_file_path = os.path.join(data_path, 'station-metadata.kml')

        self.search_strings = {
            'SMN': "SMN",
            'IMIS': "IMIS-SNOW_FLAT"
        }
        self.name_idxs = {
            'SMN': (1, 5),
            'IMIS': (0, 4)
        }

        self.metadata_dict = {}
        self.name_dict = {}
        with open(metadata_file_path) as f:
            doc = parser.parse(f)

            for e in doc.getroot().Document.Placemark:
                if self.search_strings[network_id] in str(e.styleUrl):
                    nidx = self.name_idxs[network_id]
                    station_id = str(e.name)[nidx[0]:nidx[1]]
                    station_name = str(e.name)[nidx[1]+1:]
                    station_metadata = str(e.Point.coordinates).split(',')
                    long, lat, alt = [float(met) for met in station_metadata]
                    self.metadata_dict[station_id] = [lat, long, alt]
                    self.name_dict[station_id] = station_name

        
    def __len__(self) -> int:
        return len(self.metadata_dict)

    def get_metadata(self) -> Dict[str, List[float]]:
        return self.metadata_dict
    
    def get_name_dict(self) -> Dict[str, str]:
        return self.name_dict

    def get_station_metadata(self, station_id : str) -> List[float]:
        return self.metadata_dict[station_id]
        
    def get_station_name(self, station_id : str) -> str:
        return self.name_dict[station_id]
