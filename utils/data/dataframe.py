import os
import pandas as pd
import numpy as np
from typing import List, Callable, Optional

from utils.data.grid_metadata import GridMetadata
from utils.data.misc import default_filter_fn, interpolate_missing
import pysolar

class StationDataFrame(object):
    """
    Dataframe class to handle data from IMIS stations.
    Internally it holds an instance of pandas dataframe and offers functionality for data filtering and preprocessing in addition.

    Parameters
    ----------    
        station_id : str
            ID of the station to load the data for.

        filter_fn : Callable
            Function that applies additional filtering to the data frame. Defaults to None.

        interpolate_data : bool
            Whether to interpolate missing values in the data. Defaults to False.

        subsample_data : Optional[str]
            Whether to subsample the data with frequency specified as this parameter. Optional, defaults to None.

        interpolate_gaps : Optional[int]
            Whether to interpolate gaps in the data up to size specified as this parameter. Optional, defaults to None.

        data_path : str
            Path to the data files.
    """

    def __init__(self, station_id : str, filter_fn : Callable = None, interpolate_data : bool = False, subsample_data : Optional[str] = None, interpolate_gaps : Optional[int] = None, use_default_filter : bool = False, data_path : str = '') -> None:
        super(StationDataFrame, self).__init__()

        self.station_id = station_id

        self.imis_metadata = GridMetadata('IMIS', data_path)

        # Read data from the CSV
        file_name = os.path.join(data_path, station_id + '.csv')
        df = pd.read_csv(file_name, sep=',', parse_dates=['measure_date'])
        if use_default_filter:
            df = default_filter_fn(df)
        df = df.reset_index(drop=True)
        # Make sure values are sorted chronologically
        df = df.sort_values(by='measure_date')

        # Interpolate missing data in a dataframe
        if interpolate_data:
            dates = df['measure_date'].dt.date
            df_int = df.set_index('measure_date')
            df_int = df_int.resample('30min')
            df_int = df_int.interpolate(method='linear')
            df_int = df_int.reset_index(drop=False)
            # The interpolation might have created days that did not exist, make sure to exclude these
            df_int = df_int[df_int['measure_date'].dt.date.isin(dates)] 
            df_int[['anomaly']] = df_int[['anomaly']].fillna(0)
           
            self.df = df_int
        else:
            if interpolate_gaps is not None:
                df = interpolate_missing(df, ['TA_30MIN_MEAN', 'VW_30MIN_MEAN', 'VW_30MIN_MAX', 'DW_30MIN_MEAN', 'RSWR_30MIN_MEAN', 'HS', 'RH_30MIN_MEAN', 'TSS_30MIN_MEAN', 'anomaly', 'no_snow'], interpolate_gaps)
            else:
                df = df.dropna(subset=['TA_30MIN_MEAN', 'VW_30MIN_MEAN', 'VW_30MIN_MAX', 'DW_30MIN_MEAN', 'RSWR_30MIN_MEAN', 'HS', 'RH_30MIN_MEAN', 'TSS_30MIN_MEAN'])
                df[['anomaly']] = df[['anomaly']].fillna(0)
                df[['no_snow']] = df[['no_snow']].fillna(0)
            
            self.df = df

        if subsample_data is not None:
            df['measure_date'] = df['measure_date'].dt.floor(subsample_data)
            df = df.groupby('measure_date').mean().reset_index()

        # Compute additional features for the filtered data
        self.df['time_of_day'] = self.df.measure_date.dt.hour * 2 + (self.df.measure_date.dt.minute > 0).astype(np.int32)
        self.df['day_of_year'] = self.df.measure_date.dt.dayofyear 
        self.df['time_of_day_enc_sin'] = np.sin(self.df['time_of_day'] / 48.0 * 2 * np.pi - np.pi / 2.0) 
        self.df['time_of_day_enc_cos'] = np.cos(self.df['time_of_day'] / 48.0 * 2 * np.pi - np.pi / 2.0) 
        self.df['day_of_year_enc_sin'] = np.sin(self.df['day_of_year'] / 366.0 * 2 * np.pi - np.pi / 2.0) 
        self.df['day_of_year_enc_cos'] = np.cos(self.df['day_of_year'] / 366.0 * 2 * np.pi - np.pi / 2.0) 
        self.df['solar_altitude'] = pysolar.solar.get_altitude_fast(self.imis_metadata.get_station_metadata(station_id)[0], self.imis_metadata.get_station_metadata(station_id)[1], pd.to_datetime(self.df.measure_date.values))
        self.df['solar_azimuth'] = pysolar.solar.get_azimuth_fast(self.imis_metadata.get_station_metadata(station_id)[0], self.imis_metadata.get_station_metadata(station_id)[1], pd.to_datetime(self.df.measure_date.values))
        self.df['station_id'] = [station_id] * len(self.df)

        # Apply filter if provided
        if filter_fn is not None:
            self.df = filter_fn(self.df)

    def __len__(self) -> int:
        return len(self.df.shape[0])

    def get_sensor_data(self, sensor_id : str) -> np.array:
        return self.df[sensor_id].to_numpy()

    def get_data_frame(self) -> pd.DataFrame:
        return self.df

class MultiStationDataFrame(object):
    """
    This wrapper concatenates data from all provided StationDataFrame instances into a single dataframe for convenience.

    Parameters
    ----------    
        data_frames : List[StationDataFrame]
            List of StationDataFrame instances. 
    """

    def __init__(self, data_frames : List[StationDataFrame]) -> None:
        super(MultiStationDataFrame, self).__init__()

        self.data_frames = data_frames

        # Read and filter data using a dataframe class
        self.temp_dataframes = []
        dfs = []
        for df in self.data_frames:
            dfs.append(df.get_data_frame())
        # Concatenate all sub-dataframes into the final dataframe
        self.df = pd.concat(dfs, axis=0, ignore_index=True)
        self.df = self.df.reset_index()
        
    def __len__(self) -> int:
        return len(self.df.shape[0])

    def get_sensor_data(self, sensor_id : str) -> np.array:
        return self.df[sensor_id].to_numpy()

    def get_data_frame(self) -> pd.DataFrame:
        return self.df




