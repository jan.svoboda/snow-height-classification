
import pandas as pd
from typing import List

def default_filter_fn(df : pd.DataFrame) -> pd.DataFrame:
    # TA, TA_VENTI and TA_GENAU have limits -30 ... 30 degrees celsius
    df.query('TA_30MIN_MEAN >= -75.0 and TA_30MIN_MEAN <= 75.0', inplace=True)
    df['TA_30MIN_MEAN'].clip(-40.0, 40.0, inplace=True) 
    # Windspeed has limits 0 ... 30 m/s
    # Measured windspeed > 100 is most probably an error and should be discarded 
    df.query('VW_30MIN_MEAN <= 100', inplace=True)
    df['VW_30MIN_MEAN'].clip(0.0, 30.0, inplace=True) # 0 - 30
    # Wind gust has limits 0 ... 25 m/s
    # Measured wind gust > 100 is most probably an error and should be discarded 
    df.query('VW_30MIN_MAX <= 100', inplace=True)
    df['VW_30MIN_MAX'].clip(0.0, 70.0, inplace=True) # 0 - 70
    # Wind direction can be in range 0 - 360 degrees
    df.query('DW_30MIN_MEAN >= 0.0 and DW_30MIN_MEAN <= 360.0', inplace=True)
    # Clip short-wave radiation values between 0 and 1200
    df["RSWR_30MIN_MEAN"].clip(0, 1200, inplace=True) # Clip reflected short-wave radiation
    # Snow height has limits 0 ... 600cm
    # Measured snow height > 1000cm is most probably an error and should be discarded
    df.query('HS <= 1000', inplace=True)
    df['HS'].clip(0, 700, inplace=True)
    # Relative humidity has limits 0 ... 100%
    df['RH_30MIN_MEAN'].clip(0, 100, inplace=True)
    # Measured ground temperature has limits -40 ... 40 degrees celsius
    # Measured TSS > 1000 is most probably an error and should be discarded 
    if 'TSS_30MIN_MEAN' in df.columns:
        df.query('TSS_30MIN_MEAN >= -75.0 and TSS_30MIN_MEAN <= 75.0', inplace=True)
        df['TSS_30MIN_MEAN'].clip(-40.0, 40.0, inplace=True)
    return df

def interpolate_missing(df : pd.DataFrame, feats : List[str], max_gap : int = 8) -> pd.DataFrame:
    df = df.set_index('measure_date')
    df_int = df.loc[:, feats].copy()
    df_int = df_int.resample('30min').mean()
    mask = df_int.copy()
    grp = ((mask.notnull() != mask.shift().notnull()).cumsum())
    grp['ones'] = 1
    for i in feats:
        mask[i] = (grp.groupby(i)['ones'].transform('count') <= max_gap) | df_int[i].notna()

    df_int = df_int.interpolate(method='linear')[mask]
    df_int = df_int.dropna(subset=feats)

    nonint_columns = list(set(df.columns) - set(df_int.columns))

    df = df_int.join(df.loc[:, nonint_columns])
    df = df.reset_index()
    
    return df