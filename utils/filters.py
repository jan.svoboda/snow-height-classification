"""
Reimplementation of some of the filters from MeteoIO library.

https://meteoio.slf.ch/doc-release/html/classmio_1_1FilterDeGrass.html
"""

import pandas as pd
import numpy as np
import scipy as sp
import scipy.stats as spst


class FilterDeGrass(object):
    """
    Re-implementation of summer grass measurements in snow-height data caused by the nature of an ultrasonic snow-height sensor.
    
    Parameters
    ----------
        data_frame : pd.DataFrame
            Data frame containing the data to be filtered.

        fwd_min_tss_thresh : float
            Minimum threshold for TSS value for detecting the first warm day of the year.

        fwd_min_tsg_thresh : float
            Minimum threshold for TSG value for detecting the first warm day of the year.
    """
    def __init__(self, data_frame : pd.DataFrame, fwd_min_tss_thresh : float = 1.0, fwd_min_tsg_thresh : float = 1.0) -> None:
        super(FilterDeGrass, self).__init__()

        self.fwd_min_tss_thresh = fwd_min_tss_thresh
        self.fwd_min_tsg_thresh = fwd_min_tsg_thresh

        self.data_frame = data_frame
        # We'd like to further index by timestamp, so let's make the timestamp column the data frame index
        self.data_frame = self.data_frame.set_index('measure_date')
        self.data_frame = self.data_frame.sort_index()

    def process(self, param : str):
        """
        Performs filtering of the data in the parameter column `param`.

        Parameters
        ----------
            param : str
                Name of the data column to be filtered.
        """
        tss_offset = self.get_tss_offset()

        # Find first day of the spring when min(tss) > 1 for 24 hours
        tss_warm_day, tsg_warm_day = self.find_first_warm_day()

        # Find correlation between TSS and TSG for the 7 days after the first warm day
        tss_tsg_corr, _ = self.tss_tsg_correlation(tss_warm_day)

        # Now perform filtering, one point after another
        for i in range(self.data_frame.copy().shape[0]):
            curr_date = self.data_frame.index[i]
            
            if curr_date > self.data_frame.index[tsg_warm_day] \
                or curr_date < self.data_frame.index[tss_warm_day] \
                or tsg_warm_day < tss_warm_day:
           
                self.data_frame.loc[curr_date, param] = self.filter_on_tss(self.data_frame.loc[curr_date][param], curr_date, 0.0)
            else:
                if tss_offset > 1.5 or tss_tsg_corr < 0.2:        
                    self.data_frame.loc[curr_date, param] = self.filter_on_tsg(self.data_frame.loc[curr_date][param], curr_date)
                else:
                    self.data_frame.loc[curr_date, param] = self.filter_on_tss(self.data_frame.loc[curr_date][param], curr_date, 0.0)

    def filter_on_tsg(self, value : float, day_index : pd.Timestamp):
        """
        Filter data based on TSG (Temperature Snow Ground) measurement. 
        In new datasets, TSG corresponds to TS0 (Temperature Snow at 0cm -> on the ground).

        Parameters
        ----------
            value : float
                The value to check and eventually filter.

            day_index : pd.Timestamp
                Timestamp of the day in which we are filtering.

        Returns
        -------
            The filtered value.
        """
        tsg = self.data_frame['TS0_30MIN_MEAN'].loc[day_index]

        # Compute TSG daily variance
        tsg_daily_var = self.get_tsg_daily_variance(day_index)

        if tsg > 7.0 or tsg_daily_var > 1.0:
            return 0.0

        return value

    def filter_on_tss(self, value : float, day_index : pd.Timestamp, tss_offset : float):
        """
        Filter data based on TSS (Temperature Snow Surface) measurement.

        Parameters
        ----------
            value : float
                The value to check and eventually filter.

            day_index : pd.Timestamp
                Timestamp of the day in which we are filtering.

            tss_offset : float
                Precomputed offset to add to every TSS value.

        Returns
        -------
            The filtered value.
        """
        tss_daily_mean, tss_daily_min, tss_daily_max = self.get_tss_daily_ppt(day_index)

        if tss_daily_mean + tss_offset > 0.0 and tss_daily_max + tss_offset >= 5.0:
            return 0.0    
        
        # Hack - early snow season
        if day_index.month >= 7 and day_index.month <= 12:
            if tss_daily_min + tss_offset >= 2.0:
                return 0.0
            else:
                if tss_daily_min + tss_offset >= 0.0 and value < 0.4:
                    return 0.0
                elif tss_daily_min + tss_offset < 0.0 and tss_daily_max + tss_offset >= 2.0:
                    return 0.0

        return value

    def tss_tsg_correlation(self, first_warm_day_idx : int):
        """
        Compute correlation between TSS and TSG signals from a 7 day period.

        Parameters
        ----------
            first_warm_day_idx : int
                Index of the first warm day in the dataframe. First warm day is defined by a 
                class method `find_first_warm_day`.

        Returns
        -------
            The correlation coefficient between TSS and TSG values.
        """
        first_warm_day = self.data_frame.index[first_warm_day_idx]
        last_day = pd.date_range(first_warm_day, periods=7, freq='D')[-1]
        tss = self.data_frame['TSS_30MIN_MEAN'].loc[first_warm_day:last_day]
        tsg = self.data_frame['TS0_30MIN_MEAN'].loc[first_warm_day:last_day]
        return spst.pearsonr(tss, tsg)

    def find_first_warm_day(self):
        """
        Find the first warm day of the season, e.g. the meltdown day, when all the snow on the ground melts down.

        Parameters
        ----------

        Returns
        -------
            The integer index of the first warm day of the season.
        """
        tss_warm_day = None
        tsg_warm_day = None

        for i in range(self.data_frame.shape[0]):
            curr_date = self.data_frame.index[i]
            if curr_date.month > 9:
                continue
                
            tss_min = 1e6
            tsg_min = 1e6
            for j in range(i, 0, -1):
                if self.data_frame.index[j] < curr_date - pd.Timedelta(days=1):
                    break
                tss_min = min(tss_min, self.data_frame['TSS_30MIN_MEAN'].iloc[i])
                tsg_min = min(tsg_min, self.data_frame['TS0_30MIN_MEAN'].iloc[i])

            if tss_min < 1e6 and tss_min > self.fwd_min_tss_thresh:
                tss_warm_day = i
            elif tsg_min < 1e6 and tsg_min > self.fwd_min_tsg_thresh:
                tsg_warm_day = i
            
            if tss_warm_day is not None and tsg_warm_day is not None:
                break

        return tss_warm_day, tsg_warm_day

    def get_tss_offset(self):
        """
        Compute offset for the TSS values from the data.

        Parameters
        ----------

        Returns
        -------
            Precomputed offset to add to every TSS value.
        """
        df = self.data_frame.copy()
        df = df.resample('D').median()

        hs_daily_median = df['HS'].values
        tss_daily_median = df['TSS_30MIN_MEAN'].values
        rswr_daily_10pc = self.data_frame['RSWR_30MIN_MEAN'].resample('D').quantile(0.9).values

        high_tss_days = np.bitwise_and(np.bitwise_and(hs_daily_median > 0.3, tss_daily_median > 1.0), rswr_daily_10pc > 350.0)

        return df.loc[high_tss_days]['TSS_30MIN_MEAN'].median()

    def get_tss_daily_ppt(self, day_index : pd.Timestamp):
        """
        Retrieve min, max and mean statistics for TSS values in one day.

        Parameters
        ----------
            day_index : pd.Timestamp
                Timestamp of the day for which to retrieve statistics.

        Returns
        -------
            MIN, MAX and MEAN values of TSS for a day specified by `day_index`.
        """
        tss_daily_min = self.data_frame['TSS_30MIN_MEAN'].resample('D').min()
        tss_daily_max = self.data_frame['TSS_30MIN_MEAN'].resample('D').max()
        tss_daily_mean = self.data_frame['TSS_30MIN_MEAN'].resample('D').mean()
        
        date_idx = pd.Timestamp(day_index.date())
        return tss_daily_min.loc[date_idx], tss_daily_max.loc[date_idx], tss_daily_mean.loc[date_idx]
        
    def get_tsg_daily_variance(self, day_index : pd.Timestamp):
        """
        Retrieve variance for TSS values in one day.

        Parameters
        ----------
            day_index : pd.Timestamp
                Timestamp of the day for which to retrieve statistics.

        Returns
        -------
            VAR value of TSS for a day specified by `day_index`.
        """
        tsg_daily_variance = self.data_frame['TS0_30MIN_MEAN'].resample('D').var()

        date_idx = pd.Timestamp(day_index.date())
        return tsg_daily_variance.loc[date_idx]


