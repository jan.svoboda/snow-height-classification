trainer: 
  accelerator: 'cuda'
  devices: 1
  max_epochs: 300
  accumulate_grad_batches: 1
  gradient_clip_val: null
  gradient_clip_algorithm: 'norm'
  overfit_batches: 0.0
  precision: 32

  callbacks:
    - class_path: pytorch_lightning.callbacks.EarlyStopping
      init_args:
        monitor: 'val_loss'
        patience: 50
        verbose: true
    - class_path: pytorch_lightning.callbacks.ModelCheckpoint
      init_args:
        #dirpath: ''
        filename: '{epoch}-{train_loss:.3f}-{val_loss:.3f}'
        monitor: 'val_loss'
        mode: 'min'
        save_top_k: 5
        verbose: true
    - class_path: pytorch_lightning.callbacks.LearningRateMonitor
  
  logger:
    - class_path: pytorch_lightning.loggers.MLFlowLogger
      init_args:
        experiment_name: 'lstm_classif_annot_noint_classbal_aw_paper'
        run_name: 'exp1'
        tracking_uri: file:./experiments/${trainer.logger[0].init_args.experiment_name}

data:
  class_path: ml.pl.datamodules.datamodule.DataModule
  init_args:
    train_dataset:
      class_path: ml.data.dataset.StationSequenceDataset
      init_args:
        data_subsets: 
         - ['AMD2', 'measure_date.dt.year.isin([1998, 2001, 2004, 2007, 2010, 2013, 2016, 2019, 2022])', false]
         - ['BOR2', 'measure_date.dt.year.isin([2002, 2005, 2008, 2011, 2014, 2017, 2020, 2023])', false]
         - ['FNH2', 'measure_date.dt.year.isin([2000, 2003, 2006, 2009, 2012, 2015, 2018, 2021])', false]
         - ['GLA2', 'measure_date.dt.year.isin([2001, 2004, 2007, 2010, 2013, 2016, 2019, 2022])', false]
         - ['ILI2', 'measure_date.dt.year.isin([2002, 2005, 2008, 2011, 2014, 2017, 2020, 2023])', false]
         - ['KLO3', 'measure_date.dt.year.isin([1999, 2002, 2005, 2008, 2011, 2014, 2017, 2020, 2023])', false]
         - ['RNZ2', 'measure_date.dt.year.isin([2010, 2013, 2016, 2019, 2022])', false]
         - ['TUM2', 'measure_date.dt.year.isin([2004, 2007, 2010, 2013, 2016, 2019, 2022])', false]
         - ['ARO3', 'measure_date.dt.year.isin([1998, 2000, 2003, 2006, 2009, 2012, 2015, 2018, 2021])', false]
         - ['SPN2', 'measure_date.dt.year.isin([1999, 2002, 2005, 2008, 2011, 2014, 2017, 2020, 2023])', false]
         - ['LAG3', 'measure_date.dt.year.isin([2011, 2014, 2017, 2020, 2023])', false]          
         - ['FLU2', 'measure_date.dt.year.isin([2005, 2008, 2011, 2014, 2017, 2020, 2023])', false]
         - ['FOU2', 'measure_date.dt.year.isin([2001, 2004, 2007, 2010, 2013, 2016, 2019, 2022])', false]
         - ['GUT2', 'measure_date.dt.year.isin([2000, 2003, 2006, 2009, 2012, 2015, 2018, 2021])', false]
        input_feats: ['TSS_30MIN_MEAN', 'RSWR_30MIN_MEAN', 'RH_30MIN_MEAN', 'TA_30MIN_MEAN', 'HS', 'VW_30MIN_MEAN', 'solar_altitude']
        target_feats: ['no_snow']
        seq_len: 48
        max_dist_timesteps: 48
        sampling_filter: ''
        normalization:
          - 'meanstd'
          - 'none'
        norm_inputs: 
          - [-1.1219, 85.6884, 70.0613, 1.8792, 84.2651, 1.9107, 0.2241]
          - [ 10.3191, 156.2701, 26.0700, 9.2310, 96.8068, 2.1099, 32.8813]
        norm_targets: null
        data_path: '../snow-height-classification-dataset/snow-nosnow-dataset/'

    test_dataset:
      class_path: ml.data.dataset.StationSequenceDataset
      init_args:
        data_subsets: 
          - ['STN2', 'measure_date.dt.year > 2022', false]
          - ['SHE2', 'measure_date.dt.year > 2022', false]
          - ['TRU2', 'measure_date.dt.year > 2022', false]
          - ['KLO2', 'measure_date.dt.year > 2022', false]
          - ['WFJ2', 'measure_date.dt.year > 2022', false]
          - ['SLF2', 'measure_date.dt.year > 2022', false]
        input_feats: ${data.init_args.train_dataset.init_args.input_feats}
        target_feats: ${data.init_args.train_dataset.init_args.target_feats}
        seq_len: ${data.init_args.train_dataset.init_args.seq_len}
        max_dist_timesteps: ${data.init_args.train_dataset.init_args.max_dist_timesteps}
        sampling_filter: ''
        normalization: ${data.init_args.train_dataset.init_args.normalization}
        norm_inputs: ${data.init_args.train_dataset.init_args.norm_inputs}
        norm_targets: ${data.init_args.train_dataset.init_args.norm_targets}
        data_path: ${data.init_args.train_dataset.init_args.data_path}

    batch_size: 128
    num_workers: 4
    generator_random_seed: 42

model: 
  class_path: ml.pl.modules.lstm_classif_multi_module.LSTMClassifModule
  init_args:
    model: 
      class_path: ml.models.rnn.LSTMModel
      init_args:
        input_dim: 7
        hidden_dim: 128
        num_layers: 4
        output_dim: 2
        bidirectional: false
        dropout_prob: 0.25
        predict_timestep: 47  

    loss: 
      class_path: ml.pl.losses.focal_loss.MultiClassNLLLoss
      init_args:
        gamma: 2.0
        alpha: [0.5, 1.5]
        reduction: 'mean'
      
optimizer: 
  class_path: torch.optim.AdamW
  init_args:
    lr: 0.001
    
lr_scheduler:
  class_path: torch.optim.lr_scheduler.MultiStepLR
  init_args:
    milestones: [50, 100, 150]
    gamma: 0.5
