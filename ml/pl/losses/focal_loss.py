import torch
import torch.nn as nn
import torch.nn.functional as F

from typing import Optional, Sequence, List

class FocalLoss(nn.Module):
    """
    Parameters
        ----------
        base_loss : nn.Module
            Base loss to be used in the loss computation. One of [torch.nn.functional.mse_loss, torch.nn.functional.l1_loss]. 

        activation : str
            Activation function to be used in the loss computation. One of ['tanh', 'sigmoid']. Defaults to 'tanh'.

        beta : float
            Beta parameter of the focal loss. Defaults to 0.2.

        gamma : float
            Gamma parameter of the focal loss. The higher the gamma, the more the loss focuses on the hard samples. Defaults to 1.0.

        reduction : str
            Type of aggregation for the loss. One of ['none', 'max', 'mean']. Defaults to 'mean'.
    """
    def __init__(self, base_loss : nn.Module, activation : str = 'tanh', beta : float = 0.2, gamma : float = 1.0, reduction : str = 'mean') -> None:
        super(FocalLoss, self).__init__()

        self.base_loss = base_loss
        self.activation = activation
        self.beta = beta
        self.gamma = gamma
        self.reduction = reduction


    def forward(self, y_pred : torch.Tensor, y_target : torch.Tensor, weights : torch.Tensor = None) -> torch.Tensor:
        """
        Computes focal loss for regression.

        If no weights are provided, the loss is weighted by the target values itself. 

        Parameters
        ----------
        y_pred : torch.Tensor
            Shape (batch_size, y_dim)
        
        y_target : torch.Tensor
            Shape (batch_size, y_dim)

        weights : torch.Tensor
            Shape (batch_size, y_dim)
        """

        loss = self.base_loss(y_pred, y_target)
        loss *= (torch.tanh(self.beta * torch.abs(y_pred - y_target))) ** self.gamma if self.activation == 'tanh' else \
            (2 * torch.sigmoid(self.beta * torch.abs(y_pred - y_target)) - 1) ** self.gamma
       
        if weights is not None:
            loss *= weights.expand_as(loss)
       
        if self.reduction == 'mean':
            loss = loss.mean()
        elif self.reduction == 'max':
            loss = loss.max()
    
        return loss
    

# Focal loss implementation inspired by
# https://github.com/c0nn3r/RetinaNet/blob/master/focal_loss.py
# https://github.com/doiken23/pytorch_toolbox/blob/master/focalloss2d.py
class MultiClassBCELoss(nn.Module):
    def __init__(self,
                 use_weight_mask=False,
                 use_focal_weights=False,
                 focus_param=2,
                 balance_param=0.25
                 ):
        super().__init__()

        self.use_weight_mask = use_weight_mask
        self.nll_loss = nn.BCEWithLogitsLoss()
        self.use_focal_weights = use_focal_weights
        self.focus_param = focus_param
        self.balance_param = balance_param
        
    def forward(self,
                outputs,
                targets,
                weights=None):
        # inputs and targets are assumed to be BatchxClasses
        #assert len(outputs.shape) == len(targets.shape)
        #assert outputs.size(0) == targets.size(0)
        #assert outputs.size(1) == targets.size(1)
        
        # weights are assumed to be BatchxClasses
        #assert outputs.size(0) == weights.size(0)
        #assert outputs.size(1) == weights.size(1)

        if self.use_weight_mask:
            bce_loss = F.binary_cross_entropy_with_logits(input=outputs,
                                                          target=targets,
                                                          weight=weights)            
        else:
            bce_loss = self.nll_loss(input=outputs,
                                     target=targets)
        
        if self.use_focal_weights:
            logpt = - bce_loss
            pt    = torch.exp(logpt)

            focal_loss = -((1 - pt) ** self.focus_param) * logpt
            balanced_focal_loss = self.balance_param * focal_loss
            
            return balanced_focal_loss
        else:
            return bce_loss 
        

class MultiClassNLLLoss(nn.Module):
    """ Focal Loss, as described in https://arxiv.org/abs/1708.02002.

    It is essentially an enhancement to cross entropy loss and is
    useful for classification tasks when there is a large class imbalance.
    x is expected to contain raw, unnormalized scores for each class.
    y is expected to contain class labels.

    Shape:
        - x: (batch_size, C) or (batch_size, C, d1, d2, ..., dK), K > 0.
        - y: (batch_size,) or (batch_size, d1, d2, ..., dK), K > 0.
    """

    def __init__(self,
                 alpha: Optional[List[float]] = None,
                 gamma: float = 0.,
                 reduction: str = 'mean',
                 ignore_index: int = -100):
        """Constructor.

        Args:
            alpha (Tensor, optional): Weights for each class. Defaults to None.
            gamma (float, optional): A constant, as described in the paper.
                Defaults to 0.
            reduction (str, optional): 'mean', 'sum' or 'none'.
                Defaults to 'mean'.
            ignore_index (int, optional): class label to ignore.
                Defaults to -100.
        """
        if reduction not in ('mean', 'sum', 'none'):
            raise ValueError(
                'Reduction must be one of: "mean", "sum", "none".')

        super().__init__()
        self.alpha = torch.tensor(alpha)
        self.gamma = gamma
        self.ignore_index = ignore_index
        self.reduction = reduction

        self.nll_loss = nn.NLLLoss(
            weight=self.alpha, reduction='none', ignore_index=ignore_index)

    def __repr__(self):
        arg_keys = ['alpha', 'gamma', 'ignore_index', 'reduction']
        arg_vals = [self.__dict__[k] for k in arg_keys]
        arg_strs = [f'{k}={v!r}' for k, v in zip(arg_keys, arg_vals)]
        arg_str = ', '.join(arg_strs)
        return f'{type(self).__name__}({arg_str})'

    def forward(self, x: torch.Tensor, y: torch.Tensor) -> torch.Tensor:
        if x.ndim > 2:
            # (N, C, d1, d2, ..., dK) --> (N * d1 * ... * dK, C)
            c = x.shape[1]
            x = x.permute(0, *range(2, x.ndim), 1).reshape(-1, c)
            # (N, d1, d2, ..., dK) --> (N * d1 * ... * dK,)
            y = y.view(-1)

        unignored_mask = y != self.ignore_index
        y = y[unignored_mask]
        if len(y) == 0:
            return torch.tensor(0.)
        x = x[unignored_mask]

        # compute weighted cross entropy term: -alpha * log(pt)
        # (alpha is already part of self.nll_loss)
        log_p = F.log_softmax(x, dim=-1)
        ce = self.nll_loss(log_p, y)

        # get true class column from each row
        all_rows = torch.arange(len(x))
        log_pt = log_p[all_rows, y]

        # compute focal term: (1 - pt)^gamma
        pt = log_pt.exp()
        focal_term = (1 - pt)**self.gamma

        # the full loss: -alpha * ((1 - pt)^gamma) * log(pt)
        loss = focal_term * ce

        if self.reduction == 'mean':
            loss = loss.mean()
        elif self.reduction == 'sum':
            loss = loss.sum()

        return loss