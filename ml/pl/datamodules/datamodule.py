import random
from typing import Optional, Sequence, Tuple

import numpy as np
import pytorch_lightning as pl
import torch

from torch.utils.data import Dataset, DataLoader, WeightedRandomSampler
from torch.utils.data import random_split

def worker_init_fn(id: int):
    """
    DataLoaders workers init function.
    Initialize the numpy.random seed correctly for each worker, so that
    random augmentations between workers and/or epochs are not identical.
    If a global seed is set, the augmentations are deterministic.
    https://pytorch.org/docs/stable/notes/randomness.html#dataloader
    """
    # Recently a PR fixed this in Lightning, but an override of the worker_init_fn
    # without these fixes would break it again.
    uint64_seed = torch.initial_seed()
    ss = np.random.SeedSequence([uint64_seed])
    # More than 128 bits (4 32-bit words) would be overkill.
    np.random.seed(ss.generate_state(4))
    random.seed(uint64_seed)
    # Implementation by the numpy author from here
    # https://github.com/pytorch/pytorch/issues/5059#issuecomment-817392562


class DataModule(pl.LightningDataModule):
    def __init__(self, train_dataset : Dataset, test_dataset : Dataset, val_dataset : Optional[Dataset] = None, batch_size : int = 32, num_workers : int = 1, generator_random_seed : int = 42):
        super().__init__()

        self.train_dataset = train_dataset
        self.val_dataset = val_dataset
        self.test_dataset = test_dataset

        self.batch_size = batch_size
        self.num_workers = num_workers

        self.generator_random_seed = generator_random_seed

        self.setup()

    def prepare_data(self) -> None:
        pass

    def setup(self, stage: Optional[str] = None):        
        # Here you should instantiate your datasets, you may also split the train into train and validation if needed.
        if stage is None or stage == "fit":
            if self.val_dataset is None:
                train_length = int(len(self.train_dataset) * 0.9)
                val_length = int(len(self.train_dataset) - train_length)
                self.train_dataset, self.val_dataset = random_split(self.train_dataset, [train_length, val_length], torch.Generator().manual_seed(self.generator_random_seed))

    def train_dataloader(self) -> DataLoader:
        sampler = None
        return DataLoader(
            self.train_dataset,
            shuffle=True,
            batch_size=self.batch_size,
            num_workers=self.num_workers,
            worker_init_fn=worker_init_fn,
            sampler=sampler,
        )

    def val_dataloader(self) -> DataLoader:
        return DataLoader(
            self.val_dataset,
            shuffle=False,
            batch_size=self.batch_size,
            num_workers=self.num_workers,
            worker_init_fn=worker_init_fn,
        )

    def test_dataloader(self) -> Sequence[DataLoader]:
        return [
            DataLoader(
                self.test_dataset,
                shuffle=False,
                batch_size=self.batch_size,
                num_workers=self.num_workers,
                worker_init_fn=worker_init_fn,
            )
        ]
