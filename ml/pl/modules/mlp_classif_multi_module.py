from typing import Any, Sequence, Tuple, Union

import pytorch_lightning as pl
import torch
from torch.optim import Optimizer
from torchmetrics.classification import MulticlassAccuracy

class MLPClassifModule(pl.LightningModule):
    def __init__(self, model : torch.nn.Module, loss : torch.nn.Module) -> None:
        super(MLPClassifModule, self).__init__()
        
        self.model = model
        self.loss_fn = loss

        self.acc_clas_train = MulticlassAccuracy(num_classes=2, top_k=1)
        self.acc_clas_val = MulticlassAccuracy(num_classes=2, top_k=1)
        self.acc_clas_test = MulticlassAccuracy(num_classes=2, top_k=1)

    def forward(self, batch : torch.Tensor) -> torch.Tensor:
        return self.model(batch)

    def step(self, batch: Tuple[torch.Tensor]) -> Tuple[torch.Tensor]:
        #inputs, _ = batch
        inputs, labels = batch
        y_clas = self(inputs)
        y_clas = y_clas.squeeze()

        labels = labels[:, 0].to(torch.int64)
               
        loss = self.loss_fn(y_clas, labels) 
        
        return None, y_clas, labels, loss

    def training_step(self, batch: Tuple[torch.Tensor], batch_idx: int) -> torch.Tensor:
        _, clas, labels, loss = self.step(batch)      

        self.acc_clas_train.update(clas, labels)

        self.log('train_loss', loss.item(), on_step=True, on_epoch=True)
        self.log('train_acc', self.acc_clas_train, on_step=False, on_epoch=True)

        return {'loss': loss}

    def validation_step(self, batch: Tuple[torch.Tensor], batch_idx: int) -> torch.Tensor:
        _, clas, labels, loss = self.step(batch)          
        
        self.acc_clas_val.update(clas, labels)

        self.log('val_loss', loss.item(), on_step=True, on_epoch=True)
        self.log('val_acc', self.acc_clas_val, on_step=False, on_epoch=True)

        return {'val_loss': loss}

    def test_step(self, batch: Tuple[torch.Tensor], batch_idx: int) -> torch.Tensor:
        _, clas, labels, loss = self.step(batch)        
        
        self.acc_clas_test.update(clas, labels)

        self.log('test_loss', loss.item(), on_step=True, on_epoch=True)
        self.log('test_acc', self.acc_clas_test, on_step=False, on_epoch=True)

        return {'test_loss': loss}

    def configure_optimizers(self,) -> Union[Optimizer, Tuple[Sequence[Optimizer], Sequence[Any]]]:
        """
        Choose what optimizers and learning-rate schedulers to use in your optimization.
        Normally you'd need one. But in the case of GANs or similar you might have multiple.
        Return:
            Any of these 6 options.
            - Single optimizer.
            - List or Tuple - List of optimizers.
            - Two lists - The first list has multiple optimizers, the second a list of LR schedulers (or lr_dict).
            - Dictionary, with an 'optimizer' key, and (optionally) a 'lr_scheduler'
              key whose value is a single LR scheduler or lr_dict.
            - Tuple of dictionaries as described, with an optional 'frequency' key.
            - None - Fit will run without any optimizer.
        """

        optimizer = torch.optim.Adam(self.model.parameters(), lr=1e-3)
        lr_scheduler = torch.optim.lr_scheduler.MultiStepLR(optimizer, milestones=[100, 200], gamma=0.1)
        return [optimizer], [lr_scheduler]

