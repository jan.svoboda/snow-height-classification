import torch
from torch import nn
from torch.nn.utils import weight_norm
from typing import List, Tuple
from ml.models.tcn import TemporalConvolutionalMultiresNetwork
from ml.models.mlp import MLPNet

class TCNMLPNetwork(nn.Module):
    def __init__(self, tcn_model : TemporalConvolutionalMultiresNetwork, mlp_model : MLPNet) -> None:
        super(TCNMLPNetwork, self).__init__()

        self.tcn_model = tcn_model
        self.mlp_model = mlp_model

    def forward(self, x : torch.Tensor) -> torch.Tensor:
        feat = self.tcn_model(x)
        return self.mlp_model(feat)


class TCNMLPClassifNetwork(nn.Module):
    def __init__(self, tcn_model : TemporalConvolutionalMultiresNetwork, mlp_model : MLPNet) -> None:
        super(TCNMLPClassifNetwork, self).__init__()

        self.tcn_model = tcn_model
        self.mlp_model = mlp_model

    def forward(self, x : torch.Tensor) -> torch.Tensor:
        feat = self.tcn_model(x)
        clas = self.mlp_model(feat)
        return feat, clas