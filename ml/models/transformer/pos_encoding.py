import torch
import torch.nn as nn
import math

class PositionalEncoder(nn.Module):
    """
    Transformer model contains no recurrence or convolution by default.
    Positional encoder injects information about the position of the tokens in the sequence 
    so that the model can take use of the order of the sequence as well.

    Parameters
    ----------
    dropout : float
        Dropout rate. Defaults to 0.1.

    max_seq_len : int 
        Maximum length of the input sequence. Defaults to 5000.

    hidden_dim : int
        Dimension of the output of sub-layers in the model. Defaults to 512.

    batch_first : bool
        Whether the batch is in the first dimension. Defaults to False.
    """
    def __init__(self, dropout : float = 0.1, max_seq_len : int = 5000, hidden_dim : int = 512, batch_first : bool = True) -> None:
        super(PositionalEncoder, self).__init__()

        self.hidden_dim = hidden_dim
        self.dropout = nn.Dropout(p=dropout)
        self.batch_first = batch_first
        self.len_dim = 1 if batch_first else 0

        # Generatep position vector
        pos = torch.arange(max_seq_len).unsqueeze(1)
        div_term = torch.exp(torch.arange(0, hidden_dim, 2) * (-math.log(10000.0) / hidden_dim))

        pe = torch.zeros(1, max_seq_len, hidden_dim)
        pe[0, :, 0::2] = torch.sin(pos * div_term)
        pe[0, :, 1::2] = torch.cos(pos * div_term)

        self.register_buffer('pe', pe)

    def forward(self, x : torch.Tensor) -> torch.Tensor:
        """
        Parameters
        ----------
        x : torch.Tensor 
            Shape (batch_size, enc_seq_len, dim_val) or (enc_seq_len, batch_size, dim_val)
        """
        x = x + self.pe[:, :x.size(self.len_dim), :]
        return self.dropout(x)