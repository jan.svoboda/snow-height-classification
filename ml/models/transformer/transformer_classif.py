import torch
import torch.nn as nn
from torch import nn
import torch.nn.functional as F

import ml.models.transformer.pos_encoding as pe


class ClassificationMLPPooled(nn.Module):
    def __init__(self, d_model : int, num_classes : int = 2):
        super(ClassificationMLPPooled, self).__init__()

        self.mlp = nn.Sequential(
            nn.Linear(d_model, 256), 
            nn.ReLU(), 
            nn.Linear(256, 128),
            nn.ReLU(),
            nn.Linear(128, 64), 
            nn.ReLU(),
            nn.Linear(64, num_classes)
        )

    def forward(self, x : torch.Tensor) -> torch.Tensor:
        x = self.mlp(x)
        return x


class TimeSeriesClassificationAvgPoolTransformer(nn.Module):
    """
    Implementation of transformer that can be used for time series classification.

    Uses the original transformer encoder, decoder is linear classification layer.

    Parameters
    ----------
    input_size : int
        Number of input variables. Going to be 1 for univariate inputs.
    
    batch_first : bool
        Whether batch is in the first dimension of the tensor.
        
    max_seq_len : int
        Maximum length of a sequence the model can encounter.

    hidden_dim : int
        All sub-layers in the model produced outputs of dimension hidden_dim.

    num_enc_layers : int
        Number of stacked encoder layers.

    num_att_heads : int
        Number of attention heads.

    dropout_enc : float
        Dropout rate of the encoder.

    dropout_pos_enc : float
        Dropout rate of the positional encoder.

    dim_feedforward_enc : int
        Number of neurons in the linear layer of the encoder.

    num_predicted_feats : int
        Number of features to be predicted.
    """
    def __init__(self, input_size : int, batch_first : bool = True,
                seq_len : int = 48, hidden_dim : int = 128, num_enc_layers : int = 2,
                num_att_heads : int = 4, dropout_enc : float = 0.1, 
                dropout_pos_enc : float = 0.1, dim_feedforward_enc : int = 256, 
                num_classes : int = 2) -> None:
        super(TimeSeriesClassificationAvgPoolTransformer, self).__init__()

        self.hidden_dim = hidden_dim

        ###########
        # Encoder #
        ###########
        # Input linear layer
        self.enc_input_layer = nn.Linear(input_size, hidden_dim)

        # Positional encoding layer
        self.enc_pos_layer = pe.PositionalEncoder(
            dropout=dropout_pos_enc, 
            hidden_dim=hidden_dim,
            max_seq_len=seq_len,
            batch_first=batch_first
        )
        
        # Transformer encoder layers
        # Use PyTorch TransformerEncoderLayer based on paper from Vaswani etal. (2017)
        enc_layer = nn.TransformerEncoderLayer(
            d_model=hidden_dim, 
            nhead=num_att_heads,
            dim_feedforward=dim_feedforward_enc,
            dropout=dropout_enc,
            batch_first=batch_first
        )
        # Stack the encoder layer n-times in TransformerEncoder
        # Note that using norm in TransformerEncoder is redundant while using 
        # TransformerEncoderLayer, which already does the normalization internally
        self.encoder = nn.TransformerEncoder(
            encoder_layer=enc_layer,
            num_layers=num_enc_layers,
            norm=None
        )

        ###########
        # Decoder #
        ###########
        # The decoder is just a simple linear layer
        # This form of transformer is often used for time series prediction
        self.fc_class = ClassificationMLPPooled(
            d_model=hidden_dim,
            num_classes=num_classes
        )

        # Initialize model weights
        self.init_weights()

    def init_weights(self) -> None:
        """
        Initialize network weights as suggested in the original paper.
        """
        for par in self.parameters():
            if par.dim() > 1:
                nn.init.xavier_uniform_(par)

    def forward(self, src : torch.Tensor) -> torch.Tensor:
        """
        Parameters
        ----------
            src : torch.Tensor
                Encoders output sequence. Shape (S, E) for unbatched input,
                (S, N, E) if batch_first=False or (N, S, E) when batch_first=True,
                where S is the source sequence length, N is the batch size and
                E is the number of features (1 for univariate).

        Returns
        -------
            torch.Tensor : Tensor of shape (batch_size, target_sequence_length, num_predicted_features)
        """

        # Encoding
        # Pass through the input layer
        src = self.enc_input_layer(src) # * math.sqrt(self.hidden_dim) # (btsz, src_len, hidden_dim)
        # Positional encoding
        src = self.enc_pos_layer(src) # (btsz, src_len, hidden_dim)
        # Pass through all the decoder layers
        # Masking is only needed in the encoder if the input sequences are padded
        src_out = self.encoder(
            src=src,
            mask=None
        ) # (btsz, enc_seq_len, hidden_dim) 

        # Decoding
        src_out = src_out.mean(dim=1) # Global pooling across the time dimension
        out = self.fc_class(src_out) # (btsz, num_classes)
        
        return out