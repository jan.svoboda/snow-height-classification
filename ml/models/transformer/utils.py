import torch

def generate_square_subsequent_mask(dim1 : int, dim2 : int) -> torch.Tensor:
    """
    Generates an upper-triangular matrix of -inf, with zeros on diagonal.

    Parameters
    ----------
    dim1 : int
        For both src and tgt masking, this must be target sequence lenght.

    dim2 : int
        For src masking this must be encoder sequence length (i.e. the length
        of the input sequence to the model), and for tgt masking, 
        this must be target sequence length.
    
    Returns
    -------
        A tensor of shape (dim1, dim2)
    """
    return torch.triu(torch.ones(dim1, dim2) * float('-inf'), diagonal=1)