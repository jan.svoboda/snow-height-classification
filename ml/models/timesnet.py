import torch
import torch.nn as nn
import torch.fft

import math

from typing import Generic, Tuple, List, Callable

###########################################################################
### Input data embeddings
###########################################################################

class PositionalEmbedding(nn.Module):
    """
    Positional embedding of input features.

    Parameters
    ----------
        feats_model : int
            Number of features for the model.

        max_len : int
            Maximum encoding length. Defaults to 5000.
    """
    def __init__(self, feats_model : int, max_len : int = 5000):
        super(PositionalEmbedding, self).__init__()

        # Compute the positional encoding in log space
        pe = torch.zeros(max_len, feats_model).float()
        pe.require_grad = True

        position = torch.arange(0, max_len).float().unsqueeze(1)
        div_term = torch.exp(torch.arange(0, feats_model, 2).float() * -(math.log(10000.0) / feats_model))

        pe[:, 0::2] = torch.sin(position * div_term)
        pe[:, 1::2] = torch.cos(position * div_term)

        pe = pe.unsqueeze(0)
        self.register_buffer('pe', pe)

    def forward(self, x : torch.Tensor) -> torch.Tensor:
        """
        Parameters
        ----------
            x : torch.Tensor
                Input tensor.

        Returns
        -------
            : torch.Tensor
                Output tensor. 
        """
        return self.pe[:, :x.size(1)]
    

class TokenEmbedding(nn.Module):
    """
    Token embedding of input features.

    Parameters
    ----------
        feats_in : int
            Number of input features.

        feats_model : int
            Number of features for the model.
    """
    def __init__(self, feats_in : int, feats_model : int):
        super(TokenEmbedding, self).__init__()

        self.token_conv = nn.Conv1d(
            in_channels=feats_in,
            out_channels=feats_model,
            kernel_size=3, 
            padding=1,
            padding_mode='circular',
            bias=False
        )

        for m in self.modules():
            if isinstance(m, nn.Conv1d):
                nn.init.kaiming_normal_(m.weight, mode='fan_in', nonlinearity='leaky_relu')

    def forward(self, x : torch.Tensor) -> torch.Tensor:
        """
        Parameters
        ----------
            x : torch.Tensor
                Input tensor.

        Returns
        -------
            : torch.Tensor
                Output tensor. 
        """
        return self.token_conv(x.permute(0, 2, 1)).transpose(1, 2)
    

class FixedEmbedding(nn.Module):
    """
    Fixed embedding of input features.

    Parameters
    ----------
        feats_in : int
            Number of input features.

        feats_model : int
            Number of features for the model.
    """
    def __init__(self, feats_in : int, feats_model : int):
        super(FixedEmbedding, self).__init__()

        # Compute the positional encoding in log space
        emb = torch.zeros(feats_in, feats_model).float()
        emb.require_grad = False

        position = torch.arange(0, feats_in).float().unsqueeze(1)
        div_term = torch.exp(torch.arange(0, feats_model, 2).float() * -(math.log(10000.0) / feats_model))

        emb[:, 0::2] = torch.sin(position * div_term)
        emb[:, 1::2] = torch.cos(position * div_term)

        self.emb = nn.Embedding(feats_in, feats_model)
        self.emb.weight = nn.Parameter(emb, requires_grad=False)

    def forward(self, x : torch.Tensor) -> torch.Tensor:
        """
        Parameters
        ----------
            x : torch.Tensor
                Input tensor.

        Returns
        -------
            : torch.Tensor
                Output tensor. 
        """
        return self.emb(x).detach()
    

class TemporalEmbedding(nn.Module):
    """
    Temporal embedding of input features.

    Parameters
    ----------
        feats_model : int
            Number of features for the model.

        embedding_type : str
            Embedding type. One of ['fixed', 'learnable']. Defaults to 'fixed'.

        frequency : str
            Embedding frequency. One of ['h', 't']. Defaults to 'h'.
    """
    def __init__(self, feats_model : int, embedding_type : str = 'fixed', frequency : str = 'h'):
        super(TemporalEmbedding, self).__init__()

        minute_size = 4
        hour_size = 24
        weekday_size = 7
        day_size = 32
        month_size = 13

        EmbeddingLayer = FixedEmbedding if embedding_type == 'fixed' else nn.Embedding

        if frequency == 't':
            self.minute_emb = EmbeddingLayer(minute_size, feats_model)
        self.hour_emb = EmbeddingLayer(hour_size, feats_model)
        self.weekday_emb = EmbeddingLayer(weekday_size, feats_model)
        self.day_emb = EmbeddingLayer(day_size, feats_model)
        self.month_emb = EmbeddingLayer(month_size, feats_model)

    def forward(self, x : torch.Tensor) -> torch.Tensor:
        """
        Parameters
        ----------
            x : torch.Tensor
                Input tensor.

        Returns
        -------
            : torch.Tensor
                Output tensor. 
        """
        x = x.long()

        minute_x = self.minute_emb(x[:, :, 4]) if hasattr(self, 'minute_emb') else 0.0
        hour_x = self.hour_emb(x[:, :, 3])
        weekday_x = self.weekday_emb(x[:, :, 2])
        day_x = self.day_emb(x[:, :, 1])
        month_x = self.month_emb(x[:, :, 0])

        return hour_x + weekday_x + day_x + month_x + minute_x



class DataEmbedding(nn.Module):
    """
    Embedding of the input data.

    Parameters
    ----------
        feats_in : int
            Number of input features.

        feats_model : int
            Number of features for the model.

        embedding_type : str
            Embedding type. One of ['fixed', 'learnable']. Defaults to 'fixed'.

        frequency : str
            Embedding frequency. One of ['h', 't']. Defaults to 'h'.

        dropout : float
            Input feature dropout. Defaults to 0.1.
    """
    def __init__(self, feats_in : int, feats_model : int, embedding_type : str = 'fixed', frequency : str = 'h', dropout : float = 0.1):
        super(DataEmbedding, self).__init__()

        self.value_embedding = TokenEmbedding(feats_in=feats_in, feats_model=feats_model)
        self.position_embedding = PositionalEmbedding(feats_model=feats_model)
        self.temporal_embedding = TemporalEmbedding(feats_model=feats_model, embedding_type=embedding_type, frequency=frequency)

        self.dropout = nn.Dropout(p=dropout)        

    def forward(self, x : torch.Tensor, x_mark : torch.Tensor) -> torch.Tensor:
        """
        Parameters
        ----------
            x : torch.Tensor
                Input tensor.

            x_mark : torch.Tensor
                Input tensor.

        Returns
        -------
            : torch.Tensor
                Output embedding tensor. 
        """
        x_emb = self.value_embedding(x) + self.position_embedding(x)
        if x_mark is not None:
            x_emb += self.temporal_embedding(x_mark)

        return self.dropout(x_emb)


###########################################################################
### Inception Conv Block 
###########################################################################

class InceptionBlock(nn.Module):
    """
    Inception convolutional block.

    Parameters
    ----------
        in_channels : int
            Number of input features.

        out_channels : int
            Number of output features. 

        num_layers : int
            Number of convolutional layers. Defaults to 6.

        init_weights : bool
            Whether to initialize the weights or not. Defaults to True.
    """
    def __init__(self, in_channels : int, out_channels : int, num_layers : int = 6, init_weights : bool = True):
        super(InceptionBlock, self).__init__()

        self.in_channels = in_channels
        self.out_channels = out_channels
        self.num_layers = num_layers

        layers = []
        for i in range(self.num_layers):
            layers.append(
                nn.Conv2d(
                    in_channels, 
                    out_channels, 
                    kernel_size=2 * i + 1,
                    padding=i
                )
            )
        self.layers = nn.ModuleList(layers)
        
        if init_weights:
            self._init_weights()

    def _init_weights(self):
        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                nn.init.kaiming_normal_(m.weight, mode='fan_out', nonlinearity='relu')
                if m.bias is not None:
                    nn.init.constant_(m.bias, 0)

    def forward(self, x : torch.Tensor) -> torch.Tensor:
        """
        Parameters
        ----------
            x : torch.Tensor
                Input tensor.

        Returns
        -------
            : torch.Tensor
                Output tensor. 
        """

        res_list = []
        for i in range(self.num_layers):
            res_list.append(self.layers[i](x))
        res = torch.stack(res_list, dim=-1).mean(-1)
        return res
    

###########################################################################
### TimesNet Model
###########################################################################

class PeriodExtractor(nn.Module):
    def __init__(self):
        super(PeriodExtractor, self).__init__()

    def extract_periods(self, x : torch.Tensor):
        return torch.tensor(x.shape[1]), torch.Tensor(1.0)
    
    def forward(self, x : torch.Tensor):
        return self.extract_periods(x)
    
class FFTPeriodExtractor(PeriodExtractor):
    def __init__(self, top_k : int = 2):
        super(FFTPeriodExtractor, self).__init__()

        self.top_k = top_k

    def extract_periods(self, x: torch.Tensor):
        """
        Perform FFT for a period of data.

        Data comes in shape (btsz, seq_len, channels).

        Parameters
        ----------
            x : torch.Tensor
                Input data.

            k : int
                Top frequencies to retain to create slices of signal with different periods. Defaults to 2.
        """
        
        # Perform FFT of the input to get the frequencies
        xf = torch.fft.rfft(x, dim=1)

        # Find period by amplitude
        # It is assumed that the periodic features are basically constant in different batch and channel
        # Therefore, mean is taken over the batch and channel dimension
        # This way we get a list of frequencies, where an element at position t denotes the overall amplitude at frequency (t)
        freq_list = torch.abs(xf).mean(dim=0).mean(dim=-1)
        freq_list[0] = 0

        # We get the k largest elements of frequency list (the k largest amplitudes) and their positions
        _, top_idxs_list = torch.topk(freq_list, self.top_k)
        top_idxs_list = top_idxs_list.detach().cpu().numpy()
        # Period is a list of shape (top_k, ) which records the periods of mean frequencies respectively
        period = x.shape[1] // top_idxs_list

        # Additionally to period, return tensor of shape (btsz, top_k) representing the k largest amplitudes for each data sample,
        # with all the features being averaged
        return period, torch.abs(xf).mean(-1)[:, top_idxs_list]
        
class FixedPeriodExtractor(PeriodExtractor):
    def __init__(self, in_feats : int, seq_len :int, periods : List[int]):
        super(FixedPeriodExtractor, self).__init__()

        self.in_feats = in_feats
        self.seq_len = seq_len
        self.periods = torch.tensor(periods)
        self.attention = nn.Conv1d(
            in_channels=in_feats, 
            out_channels=len(periods),
            kernel_size=seq_len,
            padding=0,
            bias=False
        )

    def extract_periods(self, x: torch.Tensor):
        """
        Data comes in shape (btsz, seq_len, channels).

        Parameters
        ----------
            x : torch.Tensor
                Input data.
        """
        
        # Compute attention over fixed periods
        period_att = nn.functional.sigmoid(self.attention(x.permute(0, 2, 1)).squeeze(dim=-1))
       
        return self.periods, period_att

class TimesBlock(nn.Module):
    """
    Building block of TimesNet.

    Performs the 2D convolutions in order to capture both inter and intra-period variations.

    It gets the base frequencies by performing FFT on the data, and reshaping the time series 
    to 2D variation respectively from the main base frequencies, followed by a 2D convolution 
    whose outputs are reshaped back and added with a weight to form the final output.

    Parameters
    ----------
        feats_model : int
            Number of input and output features.

        feats_hidden : int
            Number of hidden features.

        num_layers : int
            Number of layers in each inception block.

        seq_len : int
            Length of the input sequence.

        pred_len : int
            Length of the sequence to predict.

        period_extractor : PeriodExtractor
            Method used to extract signal periods.
    """
    def __init__(self, feats_model : int, feats_hidden  : int, num_layers : int, seq_len : int, pred_len : int, period_extractor : PeriodExtractor):
        super(TimesBlock, self).__init__()

        self.seq_len = seq_len 
        self.pred_len = pred_len    
        self.period_extractor = period_extractor

        # Parameter-efficient design
        self.conv = nn.Sequential(
            InceptionBlock(feats_model, feats_hidden, num_layers),
            nn.GELU(),
            InceptionBlock(feats_hidden, feats_model, num_layers)
        )

    def forward(self, x : torch.Tensor) -> torch.Tensor:
        """
        Parameters
        ----------
            x : torch.Tensor
                Input tensor.

        Returns
        -------
            : torch.Tensor
                Output tensor. 
        """
        btsz, seq_len, num_feats = x.size()

        # period_list is of shape (top_k, ) and denotes the k-most significant periods
        # period_weight is of shape (btsz, top_k, ) and denotes its weight (derived from the amplitude)
        period_list, period_weight = self.period_extractor(x)

        res = []
        for i in range(period_list.shape[0]):
            period = period_list[i]

            # Padding
            # In order to form a 2D map, one needs total length of the sequence combined with the part to be predicted to be divisible by period
            # Therefore, if necessary, we pad the (seq_len + pred_len) to be divisible by period 
            if (self.seq_len + self.pred_len) % period != 0:
                length = (((self.seq_len + self.pred_len) // period) + 1) * period
                padding = torch.zeros([x.shape[0], (length - (self.seq_len + self.pred_len)), x.shape[2]]).to(x.device)
                out = torch.cat([x, padding], dim=1)
            else:
                length = (self.seq_len + self.pred_len)
                out = x

            # Reshape
            # Each channel of a single piece of data needs to be a 2D variable
            # Also, 2D conv later on expects the last two dimensions to be the spatial dimensions to convolve on, therefore the permute
            out = out.reshape(btsz, length // period, period, num_feats).permute(0, 3, 1, 2).contiguous()

            # 2D conv (from 1D variation to 2D variation)
            # This extracts the intra- and inter-period variations
            out = self.conv(out)

            # Reshape back
            out = out.permute(0, 2, 3, 1).reshape(btsz, -1, num_feats)

            # Cut-off the padded part of the output and add to result
            res.append(out[:, :(self.seq_len + self.pred_len), :])

        res = torch.stack(res, dim=-1) # (btsz, seq_len, num_feats, top_k)

        # Adaptive aggregation
        # Gets normalized weights from amplitude information by using softmax
        period_weight = nn.functional.softmax(period_weight, dim=1)
        period_weight = period_weight.unsqueeze(1).unsqueeze(1).repeat(1, seq_len, num_feats, 1)
        # Perform weighted addition of the results from each period
        res = torch.sum(res * period_weight, dim=-1)

        # Residual connection
        res += x

        return res
    

class TimesNet(nn.Module):
    """
    TimesNet model as described in paper:
    https://openreview.net/pdf?id=ju_Uqw384Oq
    
    Parameters
    ----------
        task_name : str
            Which task should the model perform. One of ['long_term_forecast', 'short_term_forecast', 'anomaly_detection', 'imputation', 'classification'].

        feats_in : int
            Number of input features.

        feats_model : int
            Number of features of the inner representation.

        feats_out : int
            Number of output features.

        feats_hidden : int
            Number of hidden features for TimesBlock layers.

        num_blocks : int
            Number of TimesBlock blocks in the TimesNet network.

        num_layers : int
            Number of layers in each inception block inside of TimesBlock.

        seq_len : int
            Length of the input sequence.

        pred_len : int
            Length of the sequence to predict.

        period_extractor : PeriodExtractor
            Method used to extract signal periods.

        num_classes : int
            Only used for classification. Number of model output classes.

        embedding_type : str
            Type of the embedding for the time information. One of ['fixed', 'learnable']. Defaults to 'fixed'.

        frequency : str
            Embedding frequency. One of ['h', 't']. Defaults to 'h'.

        dropout : float
            Feature dropout probability.

    """

    def __init__(self, task_name : str, feats_in : int, feats_model : int, feats_out : int, feats_hidden  : int, num_blocks : int, num_layers : int, seq_len : int, pred_len : int, period_extractor : PeriodExtractor, num_classes : int, embedding_type : str, frequency : str, dropout : float):
        super(TimesNet, self).__init__()

        self.task_name = task_name

        self.seq_len = seq_len
        self.pred_len = pred_len

        self.net = nn.ModuleList(
            [TimesBlock(feats_model, feats_hidden, num_layers, seq_len, pred_len, period_extractor) for _ in range(num_blocks)]
        )
        self.embedding = DataEmbedding(
            feats_in, 
            feats_model,
            embedding_type,
            frequency, 
            dropout
        )

        self.num_blocks = num_blocks
        self.layer_norm = nn.LayerNorm(feats_model)

        if self.task_name == 'long_term_forecast' or self.task_name == 'short_term_forecast':
            self.predict_linear = nn.Linear(
                self.seq_len, 
                self.pred_len + self.seq_len
            )
            self.projection = nn.Linear(
                feats_model,
                feats_out,
                bias=True
            )
        elif self.task_name == 'imputation' or self.task_name == 'anomaly_detection':
            self.projection = nn.Linear(
                feats_model,
                feats_out,
                bias=True
            )
        elif self.task_name == 'classification':
            self.time_emb = TemporalEmbedding(feats_model=feats_model, embedding_type=embedding_type, frequency=frequency)
            self.activation = nn.functional.gelu
            self.dropout = nn.Dropout(dropout)
            self.projection = nn.Linear(
                feats_model * seq_len,
                num_classes
            )

    def normalize_tensor(self, inputs : torch.Tensor, mask : torch.Tensor = None) -> torch.Tensor:
        if mask is None:
            # Normalization from Non-stationary Transformer
            means = inputs.mean(1, keepdim=True).detach()
            inputs = inputs - means
            stddev = torch.sqrt(torch.var(inputs, dim=1, keepdim=True, unbiased=False) + 1e-5)
            inputs /= stddev
        else:
            means = torch.sum(inputs, dim=1) / torch.sum(mask == 1, dim=1)
            means = means.unsqueeze(1).detach()
            inputs = inputs - means
            stddev = torch.sqrt(torch.sum(inputs * inputs, dim=1) / torch.sum(mask == 1, dim=1) + 1e-5)
            stddev = stddev.unsqueeze(1).detach()
            inputs /= stddev

        return inputs, means, stddev
    
    def unnormalize_tensor(self, inputs : torch.Tensor, means : torch.Tensor, stddev : torch.Tensor):
        inputs = inputs * (stddev[:, 0, :].unsqueeze(1).repeat(1, self.pred_len + self.seq_len, 1))
        inputs = inputs + (means[:, 0, :].unsqueeze(1).repeat(1, self.pred_len + self.seq_len, 1))
        return inputs

    def forecast(self, x_feats : torch.Tensor, x_time_feats : torch.Tensor) -> torch.Tensor:
        # Normalization from Non-stationary Transformer
        x_feats, means, stddev = self.normalize_tensor(x_feats)

        # Embedding
        enc_out = self.embedding(x_feats, x_time_feats) # (btsz, seq_len, num_feats)
        enc_out = self.predict_linear(enc_out.permute(0, 2, 1)).permute(0, 2, 1) # align temporal dimensions
       
        # TimesNet
        for i in range(self.num_blocks):
            enc_out = self.layer_norm(self.net[i](enc_out))

        # Project back
        dec_out = self.projection(enc_out)
       
        # De-Normalization from Non-stationary Transformer
        dec_out = self.unnormalize_tensor(dec_out, means, stddev)

        return dec_out
    
    def imputation(self, x_feats : torch.Tensor, x_time_feats : torch.Tensor, mask : torch.Tensor) -> torch.Tensor:
        # Normalization from Non-stationary Transformer
        x_feats, means, stddev = self.normalize_tensor(x_feats, mask)

        # Embedding
        enc_out = self.embedding(x_feats, x_time_feats) # (btsz, seq_len, num_feats)

        # TimesNet
        for i in range(self.num_blocks):
            enc_out = self.layer_norm(self.net[i](enc_out))
        # Project back
        dec_out = self.projection(enc_out)

        # De-Normalization from Non-stationary Transformer
        dec_out = self.unnormalize_tensor(dec_out, means, stddev)

        return dec_out
    
    def anomaly_detection(self, x_feats : torch.Tensor) -> torch.Tensor:
        # Normalization from Non-stationary Transformer
        x_feats, means, stddev = self.normalize_tensor(x_feats)

        # Embedding
        enc_out = self.embedding(x_feats, None) # (btsz, seq_len, num_feats)

        # TimesNet
        for i in range(self.num_blocks):
            enc_out = self.layer_norm(self.net[i](enc_out))
        # Project back
        dec_out = self.projection(enc_out)

        # De-Normalization from Non-stationary Transformer
        dec_out = self.unnormalize_tensor(dec_out, means, stddev)

        return dec_out
    
    def classification(self, x_feats : torch.Tensor, x_time_feats : torch.Tensor) -> torch.Tensor:
        # Embedding
        enc_out = self.embedding(x_feats, None) # (btsz, seq_len, num_feats)

        # TimesNet
        for i in range(self.num_blocks):
            enc_out = self.layer_norm(self.net[i](enc_out))
            
        # Output
        output = self.activation(enc_out)
        output = self.dropout(output)

        # zero-out padding embeddings
        # The primary role of x_time_feats is to zero-out the embeddings for padding positions 
        # in the output tensor through elementwise multiplication, helping the model to focus 
        # on meaningful data while discarding the padding
        x_time_emb = self.time_emb(x_time_feats)
        output = output * x_time_emb
        output = output.reshape(output.shape[0], -1) # (btsz, seq_len * num_feats)
        output = self.projection(output) # (btsz, num_classes)
        
        return output
    
    def forward(self, x_feats : torch.Tensor, x_time_feats : torch.Tensor = None, mask : torch.Tensor = None) -> torch.Tensor:
        if self.task_name == 'long_term_forecast' or self.task_name == 'short_term_forecast':
            dec_out = self.forecast(x_feats, x_time_feats)
            return dec_out[:, -self.pred_len:, :] # (btsz, seq_len, num_feats)
        elif self.task_name == 'imputation':
            dec_out = self.imputation(x_feats, x_time_feats, mask)
            return dec_out # (btsz, seq_len, num_feats)
        elif self.task_name == 'anomaly_detection':
            dec_out = self.anomaly_detection(x_feats)
            return dec_out # (btsz, seq_len, num_feats)
        elif self.task_name == 'classification':
            dec_out = self.classification(x_feats, x_time_feats)
            return dec_out # (btsz, num_classes)
        else:
            return None
      