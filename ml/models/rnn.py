from turtle import forward
import torch
import torch.nn as nn


class RecurrentModel(nn.Module):
    """
    Base class for reccurent model wrappers.

    Parameters
    ----------
    input_dim : int 
        The number of input features
    
    hidden_dim : int
        The number of features in each layer

    num_layers : int
        The number of layers in the RNN

    bidirectional : bool
        Whether to use a bidirectional RNN.

    output_dim : int
        The number of output features

    dropout_prob : float
        Probablity of feature dropout

    predict_timestep : int
        Index of the timestep to predict in the series. If -1, the whole series is predicted. Defaults to -1.
    """
    def __init__(self, input_dim : int, hidden_dim : int, num_layers : int, output_dim : int, bidirectional : bool, dropout_prob : float, predict_timestep : int = -1) -> None:
            super(RecurrentModel, self).__init__()

            self.input_dim = input_dim
            self.hidden_dim = hidden_dim
            self.num_layers = num_layers
            self.output_dim = output_dim
            self.bidirectional = bidirectional
            self.dropout_prob = dropout_prob
            self.predict_timestep = predict_timestep

            # Placeholder for the recurrent neural network
            self.recurrent_model = None
                
            # Output fully connected layer
            mult = 2 if self.bidirectional else 1
            self.lin_out = nn.Linear(hidden_dim * mult, output_dim)

    def forward(self, x : torch.Tensor, h0 : torch.Tensor = None) -> torch.Tensor:
        """
        Parameters
        ----------
        x : torch.Tensor
            The input tensor of shape (batch_size, seq_len, input_dim)
        
        Returns
        -------
            torch.Tensor : The output tensor of shape (batch_size, output_dim)
        """

        mult = 2 if self.bidirectional else 1

        # Initializing hidden state for the first input with zeros
        if h0 is None:
            h0 = torch.zeros(self.num_layers * mult, x.size(0), self.hidden_dim, device=x.device).requires_grad_()

        # We are doing truncated backpropagation through time (BPTT)
        # We therefore need to detach, becaues if we don't, we will backprop all the way
        # to the start even with the next batch
        # Forward propagation by passing in the input and hidden state into the model
        out, hn = self.recurrent_model(x, h0.detach())

        # Take the last element in the sequence produced by RNN and feed it to the output layer
        if self.predict_timestep >= 0:
            out = out[:, self.predict_timestep, :]
        # Convert the final state into the desired output
        out = self.lin_out(out)
        
        return out


class RNNModel(RecurrentModel):
    """
    Recurrent neural network (RNN) model wrapper.
    
    Parameters
    ----------
    input_dim : int 
        The number of input features
    
    hidden_dim : int
        The number of features in each layer

    num_layers : int
        The number of layers in the RNN

    output_dim : int
        The number of output features

    bidirectional : bool
        Whther to use a bidirectional RNN.

    dropout_prob : float
        Probablity of feature dropout
    """

    def __init__(self, input_dim : int, hidden_dim : int, num_layers : int, output_dim : int, bidirectional : bool, dropout_prob : float, predict_timestep : int = -1) -> None:
        super(RNNModel, self).__init__(input_dim, hidden_dim, num_layers, output_dim, bidirectional, dropout_prob, predict_timestep)

        self.recurrent_model = nn.RNN(
            input_dim,
            hidden_dim,
            num_layers,
            batch_first=True,
            bidirectional=self.bidirectional,
            dropout=dropout_prob
        )

    def forward(self, x : torch.Tensor, h0 : torch.Tensor = None) -> torch.Tensor:
        return super(RNNModel, self).forward(x, h0)


class RNNAggModel(RecurrentModel):
    """
    Recurrent neural network (RNN) model wrapper.
    
    Parameters
    ----------
    input_dim : int 
        The number of input features
    
    hidden_dim : int
        The number of features in each layer

    num_layers : int
        The number of layers in the RNN

    output_dim : int
        The number of output features

    bidirectional : bool
        Whther to use a bidirectional RNN.

    dropout_prob : float
        Probablity of feature dropout
    """

    def __init__(self, input_dim : int, hidden_dim : int, num_layers : int, output_dim : int, bidirectional : bool, dropout_prob : float, predict_timestep : int = -1) -> None:
        super(RNNAggModel, self).__init__(input_dim, hidden_dim, num_layers, output_dim, bidirectional, dropout_prob, predict_timestep)

        self.recurrent_model = nn.RNN(
            input_dim,
            hidden_dim,
            num_layers,
            batch_first=True,
            bidirectional=self.bidirectional,
            dropout=dropout_prob
        )

    def forward(self, x : torch.Tensor, h0 : torch.Tensor = None) -> torch.Tensor:
        return super(RNNAggModel, self).forward(x, h0).sum(dim=1)


class LSTMModel(RecurrentModel):
    """
    Long Short-Term Memory neural network (LSTM) model wrapper.
    
    Parameters
    ----------
    input_dim : int 
        The number of input features
    
    hidden_dim : int
        The number of features in each layer

    num_layers : int
        The number of layers in the RNN

    output_dim : int
        The number of output features

    bidirectional : bool
        Whether to use bidirectional RNN.

    dropout_prob : float
        Probablity of feature dropout. 
    """

    def __init__(self, input_dim : int, hidden_dim : int, num_layers : int, output_dim : int, bidirectional : bool, dropout_prob : float, predict_timestep : int = -1) -> None:
        super(LSTMModel, self).__init__(input_dim, hidden_dim, num_layers, output_dim, bidirectional, dropout_prob, predict_timestep)

        self.recurrent_model = nn.LSTM(
            input_dim,
            hidden_dim,
            num_layers,
            batch_first=True,
            bidirectional=bidirectional,
            dropout=dropout_prob
        )

    def forward(self, x : torch.Tensor, h0 : torch.Tensor = None, c0 : torch.Tensor = None) -> torch.Tensor:
        """
        Parameters
        ----------
        x : torch.Tensor
            The input tensor of shape (batch_size, seq_len, input_dim)
        
        Returns
        -------
            torch.Tensor : The output tensor of shape (batch_size, output_dim)
        """

        mult = 2 if self.bidirectional else 1

        # Initialize hidden state for the first input with zeros
        if h0 is None:
            h0 = torch.zeros(self.num_layers * mult, x.size(0), self.hidden_dim, device=x.device).requires_grad_()

        # Initialize cell state for the first input with zeros
        if c0 is None:
            c0 = torch.zeros(self.num_layers * mult, x.size(0), self.hidden_dim, device=x.device).requires_grad_()

        # Forward propagation by passing in the input and hidden state into the model
        out, (hn, cn) = self.recurrent_model(x, (h0.detach(), c0.detach()))

        # Take the last element in the sequence produced by RNN and feed it to the output layer
        if self.predict_timestep >= 0:
            out = out[:, self.predict_timestep, :]
        # Convert the final state into the desired output
        out = self.lin_out(out)
        
        return out, hn, cn


class GRUModel(RecurrentModel):
    """
    Gated Recurrent Unit neural network (GRU) model wrapper.
    
    Parameters
    ----------
    input_dim : int 
        The number of input features
    
    hidden_dim : int
        The number of features in each layer

    num_layers : int
        The number of layers in the RNN

    output_dim : int
        The number of output features

    bidirectional : bool
        Whether to use bidirectional RNN,

    dropout_prob : float
        Probablity of feature dropout.
    """

    def __init__(self, input_dim : int, hidden_dim : int, num_layers : int, output_dim : int, bidirectional : bool, dropout_prob : float, predict_timestep : int = -1) -> None:
        super(GRUModel, self).__init__(input_dim, hidden_dim, num_layers, output_dim, bidirectional, dropout_prob, predict_timestep)

        self.recurrent_model = nn.GRU(
            input_dim,
            hidden_dim,
            num_layers,
            batch_first=True,
            bidirectional=bidirectional,
            dropout=dropout_prob
        )

    def forward(self, x : torch.Tensor, h0 : torch.Tensor = None) -> torch.Tensor:
        return super(GRUModel, self).forward(x, h0)
    

class ConditionalRNN(nn.Module):
    """
    Recurrent neural network model conditioned with inputs processed by an additional MLP.
    
    Parameters
    ----------
        rnn_model : nn.Module
            RNN network representing the temporal part of the model.

        mlp_model : nn.Module
            MLP network representing the conditioning part of the model.

        output_dim : int
            The number of output features
    """

    def __init__(self, rnn_model : nn.Module, mlp_model : nn.Module, output_dim : int) -> None:
        super(ConditionalRNN, self).__init__()

        self.rnn_model = rnn_model
        self.mlp_model = mlp_model
        self.output_dim = output_dim

        # Encode altitude representation
        num_embs = 48
        emb_dim = 16
        self.altitude_emb = torch.nn.Embedding(num_embs, emb_dim)
        self.net_alt = nn.Sequential(
            nn.Linear(self.mlp_model.output_dim + emb_dim, self.mlp_model.output_dim),
        )

        # Encode previous danger level representation
        num_embs = 4
        emb_dim = 4
        self.pdang_emb = torch.nn.Embedding(num_embs, emb_dim)
        #self.net_pdang = nn.Sequential(
        #    nn.Linear(self.mlp_model.output_dim  + emb_dim, self.mlp_model.output_dim),
        #)

        self.net_out = nn.Sequential(
            nn.Linear(self.mlp_model.output_dim , output_dim),
        )

    def forward(self, x : torch.Tensor, alt : torch.Tensor, dang : torch.Tensor) -> torch.Tensor:
        """
        Parameters
        -----------
            x_t : torch.Tensor
                The temporal data which are fed to the RNN for processing.

            cond : torch.Tensor
                The static conditioning vector that is fed to the conditioning MLP model.
        """

        btsz = x.shape[0]

        # Encode altitude and avalanche danger
        e_alt = self.altitude_emb(alt)[:, :, 0, :]
        e_dang = self.pdang_emb(dang)[:, :, 0, :]

        x_past = x[:, :-1, :]
        e_alt_past = e_alt[:, :-1, :]
        e_dang_past = e_dang[:, :-1, :]

        z = self.rnn_model(torch.cat([x_past, e_alt_past, e_dang_past], dim=2)).reshape(btsz, -1)

        x_curr = x[:, -1, :]
        z = self.mlp_model(torch.cat([x_curr, z], dim=1))

        e_alt_curr = e_alt[:, -1, :]
        z = self.net_alt(torch.cat([z, e_alt_curr], dim=1))
        
        #z = self.net_pdang(torch.cat([z, e_pdang[:, 0, :]], dim=1))
    
        return self.net_out(z)




class ConditionalRNNModel(nn.Module):
    """
    Recurrent neural network model conditioned with inputs processed by an additional MLP.
    
    Parameters
    ----------
        rnn_model : nn.Module
            RNN network representing the temporal part of the model.

        mlp_model : nn.Module
            MLP network representing the conditioning part of the model.

        output_dim : int
            The number of output features
    """

    def __init__(self, rnn_model : nn.Module, mlp_model : nn.Module, output_dim : int) -> None:
        super(ConditionalRNNModel, self).__init__()

        self.rnn_model = rnn_model
        self.mlp_model = mlp_model
        self.output_dim = output_dim

        # Encode previous danger level representation
        num_embs = 5
        emb_dim = 4
        self.pdang_emb = torch.nn.Embedding(num_embs, emb_dim)
      
        self.net_out = nn.Sequential(
            nn.Linear(self.mlp_model.output_dim, output_dim),
        )

    def forward(self, x : torch.Tensor, dang : torch.Tensor) -> torch.Tensor:
        """
        Parameters
        -----------
            x_t : torch.Tensor
                The temporal data which are fed to the RNN for processing.

            cond : torch.Tensor
                The static conditioning vector that is fed to the conditioning MLP model.
        """

        btsz = x.shape[0]

        # Encode altitude and avalanche danger
        e_dang = self.pdang_emb(dang)

        z = self.rnn_model(x).reshape(btsz, -1)
        z = self.mlp_model(torch.cat([z, e_dang], dim=1))
            
        return self.net_out(z)





    

    





