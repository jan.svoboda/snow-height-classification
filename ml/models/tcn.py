import torch
from torch import nn
from torch.nn.utils import weight_norm
from typing import List, Tuple, Optional

class Cutoff1d(nn.Module):
    """
    Cuts off last cutoff_size elements from the sequence.

    Parameters
    ----------
        cutoff_size : int
            How much to cutoff from the sequence.
    """
    def __init__(self, cutoff_size : int) -> None:
        super(Cutoff1d, self).__init__()

        self.cutoff_size = cutoff_size

    def forward(self, x : torch.Tensor) -> torch.Tensor:
        """
        Parameters
        ----------
            x : torch.Tensor
                Tensor to be cut-off of input shape (batch_size, input_dim, seq_len).
        """
        return x[:, :, :-self.cutoff_size].contiguous()

class TemporalBlock(nn.Module):
    """
    Building block of the Temporal Convolutional Network (TCN).

    Parameters
    ----------
        input_dim : int
            Number of input channels.

        hidden_dim : int
            Number of hidden channels.

        output_dim : int
            Number of output channels.

        kernel_size : int
            Kernel size of the convolutional layers.

        stride : int
            Stride of the convolutional layers.

        dilation : int
            Dilation of the convolutional layers.

        padding : int
            Padding of the convolutional layers.

        dropout : float
            Dropout probability.

        residual : bool
            Whether to have a residual connection from input to output. Defaults to True.
    """
    def __init__(self, input_dim : int, hidden_dim : int, output_dim: int, kernel_size : int, stride : int, dilation : int, padding : int, dropout : float = 0.2, residual : bool = True) -> None:
        super(TemporalBlock, self).__init__()

        self.residual = residual

        self.conv1 = weight_norm(nn.Conv1d(input_dim, hidden_dim, kernel_size, stride=stride, padding=padding, dilation=dilation))
        self.conv2 = weight_norm(nn.Conv1d(hidden_dim, hidden_dim, kernel_size, stride=stride, padding=padding, dilation=dilation))
        #self.conv1 = nn.Conv1d(input_dim, hidden_dim, kernel_size, stride=stride, padding=padding, dilation=dilation)
        #self.conv2 = nn.Conv1d(hidden_dim, hidden_dim, kernel_size, stride=stride, padding=padding, dilation=dilation)

        self.net = nn.Sequential(
            self.conv1,
            Cutoff1d(padding),
            #nn.BatchNorm1d(hidden_dim),
            nn.ReLU(),
            nn.Dropout(dropout),

            self.conv2,
            Cutoff1d(padding),
            #nn.BatchNorm1d(hidden_dim),
            nn.ReLU(),
            nn.Dropout(dropout),
        )

        self.downsample_in = nn.Conv1d(input_dim, output_dim, 1) if (input_dim != output_dim and self.residual) else None
        self.downsample_hid = nn.Conv1d(hidden_dim, output_dim, 1) if hidden_dim != output_dim else None
        self.relu = nn.ReLU()

        self.init_weights()

    def init_weights(self) -> None:
        """
        Utility function to initialize weights of the convolutional layers in the network.
        """
        self.conv1.weight.data.normal_(0, 0.01)
        self.conv2.weight.data.normal_(0, 0.01)
        if self.downsample_in is not None:
            self.downsample_in.weight.data.normal_(0, 0.01)
        if self.downsample_hid is not None:
            self.downsample_hid.weight.data.normal_(0, 0.01)

    def forward(self, x : torch.Tensor) -> torch.Tensor:
        """
        Parameters
        ----------
            x : torch.Tensor
                Tensor to be fed to the neural network of shape (batch_size, input_dim, seq_len).
        """
        hid = self.net(x)
        out = hid if self.downsample_hid is None else self.downsample_hid(hid)

        if self.residual:
            res = x if self.downsample_in is None else self.downsample_in(x)
            out = res + out

        return self.relu(out)


class TemporalConvolutionalNetwork(nn.Module):
    """
    Implementation of the Temporal Convolutional Network (TCN).
    Bai S. etal., An Empirical Evaluation of Generic Convolutional and Recurrent Networks for Sequence Modeling, 2018
    https://arxiv.org/abs/1803.01271

    Blogpost explaining TCNs:
    https://unit8.com/resources/temporal-convolutional-networks-and-forecasting/

    Equation for the minimum number of layers to cover the entire sequence history:
    num_layers = log2((seq_len - 1)*(dilation_base-1)/(kernel_size-1)+1)

    Parameters
    ----------
        num_inputs : int
            Number of input channels.

        num_outputs : int
            Number of output channels.

        num_channels : List[Tuple[int]]
            List of tuples (num_hidden, num_outputs) in each layer of TCN.

        kernel_size : int
            Kernel size of the convolutional layers.

        dropout : float
            Dropout probability.

        predict_timestep : int
            Index of the timestep to predict in the series. If -1, the whole series is predicted. Defaults to -1.

        output_linear : bool
            Attach output linear layer.

        output_activation : nn.Module
            Nonlinearity to apply to the outputs. Defaults to nn.Identity.

        residual : bool
            Whether to have a residual connection from input to output in each block. Defaults to True.

        inv_dilation : bool
            Invert the order of dilation sizes. Defaults to False.
    """
    def __init__(self, num_inputs : int, num_outputs : int, num_channels : List[Tuple[int, int]], kernel_size : int = 2, dropout : float = 0.2, predict_timestep : int = -1, output_linear : bool = True, output_activation : nn.Module = nn.Identity(), residual : bool = True, inv_dilation : bool = False) -> None:
        super(TemporalConvolutionalNetwork, self).__init__()

        self.inv_dilation = inv_dilation
        self.output_linear = output_linear
        self.output_activation = output_activation
        self.predict_timestep = predict_timestep

        layers = []
        num_levels = len(num_channels)
        for i in range(num_levels):
            dilation_size = 2**(num_levels - i - 1) if self.inv_dilation else 2**i
            input_dim = num_inputs if i == 0 else num_channels[i-1][1]
            hidden_dim = num_channels[i][0]
            output_dim = num_channels[i][1]
            layers.append(TemporalBlock(input_dim, hidden_dim, output_dim, kernel_size, stride=1, dilation=dilation_size,
                padding=(kernel_size-1) * dilation_size, dropout=dropout, residual=residual))

        self.net = nn.Sequential(*layers)

        if self.output_linear:
            self.linear = nn.Linear(num_channels[-1][1], num_outputs)

    def forward(self, x : torch.Tensor) -> torch.Tensor:
        """
        Parameters
        ----------
            x : torch.Tensor
                Tensor to be fed to the neural network of shape (batch_size, seq_len, input_dim).
        """
        # TCN needs inputs of shape (batch_size, input_dim, seq_len), therefore we need to permute
        x = x.permute(0, 2, 1)
        # Conver the TCN output back to shape (batch_size, seq_len, output_dim)
        out = self.net(x).permute(0, 2, 1)
        if self.output_linear:
            if self.predict_timestep < 0:
                return self.output_activation(self.linear(out))
            else:
                return self.output_activation(self.linear(out[:, self.predict_timestep, :]))
        else:
            return self.output_activation(out)

class TemporalConvolutionalMultiresNetwork(nn.Module):
    """
    Implementation of the Temporal Convolutional Network (TCN).
    Bai S. etal., An Empirical Evaluation of Generic Convolutional and Recurrent Networks for Sequence Modeling, 2018
    https://arxiv.org/abs/1803.01271

    Blogpost explaining TCNs:
    https://unit8.com/resources/temporal-convolutional-networks-and-forecasting/

    Equation for the minimum number of layers to cover the entire sequence history:
    num_layers = log2((seq_len - 1)*(dilation_base-1)/(kernel_size-1)+1)

    Parameters
    ----------
        num_inputs : int
            Number of input channels.

        num_outputs : int
            Number of output channels.

        num_channels : List[Tuple[int]]
            List of tuples (num_hidden, num_outputs) in each layer of TCN.

        kernel_size : int
            Kernel size of the convolutional layers.

        num_res : int
            How many resolutions to use to craete the output.

        dropout : float
            Dropout probability.

        predict_timestep : int
            Index of the timestep to predict in the series. If -1, the whole series is predicted. Defaults to -1.

        output_linear : bool
            Attach output linear layer.

        output_activation : nn.Module
            Nonlinearity to apply to the outputs. Defaults to nn.Identity.

        residual : bool
            Whether to have a residual connection from input to output in each block. Defaults to True.

        inv_dilation : bool
            Invert the order of dilation sizes. Defaults to False.
    """
    def __init__(self, num_inputs : int, num_outputs : int, num_channels : List[Tuple[int, int]], kernel_size : int = 2, num_res : int = 2, dropout : float = 0.2, predict_timestep : Optional[int] = None,  output_linear : bool = True, output_activation : nn.Module = nn.Identity(), residual : bool = True, inv_dilation : bool = False) -> None:
        super(TemporalConvolutionalMultiresNetwork, self).__init__()

        self.num_res = num_res
        self.inv_dilation = inv_dilation
        self.predict_timestep = predict_timestep
        self.output_linear = output_linear
        self.output_activation = output_activation
        self.num_outputs = num_outputs

        self.layers = []
        num_levels = len(num_channels)
        for i in range(num_levels):
            dilation_size = 2**(num_levels - i - 1) if self.inv_dilation else 2**i
            input_dim = num_inputs if i == 0 else num_channels[i-1][1]
            hidden_dim = num_channels[i][0]
            output_dim = num_channels[i][1]
            self.layers.append(TemporalBlock(input_dim, hidden_dim, output_dim, kernel_size, stride=1, dilation=dilation_size,
                padding=(kernel_size-1) * dilation_size, dropout=dropout, residual=residual))
        self.layers = nn.ModuleList(self.layers)

        # Output is created from features of last 'num_res' channels
        num_chans = 0
        for chans in num_channels[-num_res:]:
            num_chans += chans[1]

        if self.output_linear:
            self.linear = nn.Linear(num_chans, num_outputs)

    def forward(self, x : torch.Tensor) -> torch.Tensor:
        """
        Parameters
        ----------
            x : torch.Tensor
                Tensor to be fed to the neural network of shape (batch_size, seq_len, input_dim).
        """
        # TCN needs inputs of shape (batch_size, input_dim, seq_len), therefore we need to permute
        x = x.permute(0, 2, 1)
        outs = [x]
        for layernet in self.layers:
            outs.append(layernet(outs[-1]))
        
        out = torch.cat(outs[-self.num_res:], dim=1)

        # Conver the TCN output back to shape (batch_size, seq_len, output_dim)
        out = out.permute(0, 2, 1)
        if self.output_linear:
            if self.predict_timestep is None:
                return self.output_activation(self.linear(out))
            else:
                return self.output_activation(self.linear(out[:, self.predict_timestep, :]))
        else:
            return self.output_activation(out)


class TemporalConvolutionalMultiresSequenceNetwork(TemporalConvolutionalMultiresNetwork):
    """
    Implementation of the Temporal Convolutional Network (TCN).
    Bai S. etal., An Empirical Evaluation of Generic Convolutional and Recurrent Networks for Sequence Modeling, 2018
    https://arxiv.org/abs/1803.01271

    Blogpost explaining TCNs:
    https://unit8.com/resources/temporal-convolutional-networks-and-forecasting/

    Equation for the minimum number of layers to cover the entire sequence history:
    num_layers = log2((seq_len - 1)*(dilation_base-1)/(kernel_size-1)+1)

    This version predicts not only one time-step, but a time-series which is defined as follows:
     We consider: 
      seq_len = backcast_len + forecast_len
     Then we define:
      predict_timestep = backcast_len
     And we return the following subsequence:
      return out[:, predict_timestep:, :]

    Parameters
    ----------
        num_inputs : int
            Number of input channels.

        num_outputs : int
            Number of output channels.

        num_channels : List[Tuple[int]]
            List of tuples (num_hidden, num_outputs) in each layer of TCN.

        kernel_size : int
            Kernel size of the convolutional layers.

        num_res : int
            How many resolutions to use to craete the output.

        dropout : float
            Dropout probability.

        predict_timestep : int
            Index of the timestep to predict in the series. If -1, the whole series is predicted. Defaults to -1.

        output_linear : bool
            Attach output linear layer.

        output_activation : nn.Module
            Nonlinearity to apply to the outputs. Defaults to nn.Identity.

        residual : bool
            Whether to have a residual connection from input to output in each block. Defaults to True.

        inv_dilation : bool
            Invert the order of dilation sizes. Defaults to False.
    """
    def __init__(self, num_inputs : int, num_outputs : int, num_channels : List[Tuple[int, int]], kernel_size : int = 2, num_res : int = 2, dropout : float = 0.2, predict_timestep : int = -1,  output_linear : bool = True, output_activation : nn.Module = nn.Identity(), residual : bool = True, inv_dilation : bool = False) -> None:
        super(TemporalConvolutionalMultiresSequenceNetwork, self).__init__(num_inputs, num_outputs, num_channels, kernel_size, num_res, dropout, predict_timestep,  output_linear, output_activation, residual, inv_dilation)

    def forward(self, x : torch.Tensor) -> torch.Tensor:
        """
        Parameters
        ----------
            x : torch.Tensor
                Tensor to be fed to the neural network of shape (batch_size, seq_len, input_dim).
        """
        # TCN needs inputs of shape (batch_size, input_dim, seq_len), therefore we need to permute
        x = x.permute(0, 2, 1)
        outs = [x]
        for layernet in self.layers:
            outs.append(layernet(outs[-1]))
        
        out = torch.cat(outs[-self.num_res:], dim=1)

        # Conver the TCN output back to shape (batch_size, seq_len, output_dim)
        out = out.permute(0, 2, 1)
        if self.output_linear:
            if self.predict_timestep < 0:
                return self.output_activation(self.linear(out))
            else:
                return self.output_activation(self.linear(out[:, self.predict_timestep:, :]))
        else:
            return self.output_activation(out)


class TemporalConvolutionalAutoencoder(nn.Module):
    """
    Implementation of the TemporalConvolutionalAutoencoder (TCN-AE).
    Thill, Markus; Konen, Wolfgang; Bäck, Thomas (2020) Time Series Encodings with Temporal Convolutional Networks 
    http://www.gm.fh-koeln.de/ciopwebpub/Thill20a.d/bioma2020-tcn.pdf

    Parameters
    ----------
        num_inputs : int
            Number of input channels.

        num_outputs : int
            Number of output channels.

        num_channels : List[Tuple[int]]
            List of tuples (num_hidden, num_outputs) in each layer of TCN.

        kernel_size : int
            Kernel size of the convolutional layers.

        dropout : float
            Dropout probability.
    """
    def __init__(self, num_inputs : int, num_outputs : int, num_channels : List[Tuple[int, int]], kernel_size : int = 2, dropout : float = 0.2) -> None:
        super(TemporalConvolutionalAutoencoder, self).__init__()

        self.sampling_factor = 4

        # Encoder
        self.enc_tcn = TemporalConvolutionalNetwork(num_inputs, num_channels[-1][1], num_channels, kernel_size, dropout, predict_timestep=-1, output_linear=False)
        self.ecn_conv1d = nn.Conv1d(num_channels[-1][1], num_channels[-1][1], kernel_size=1, padding='same')
        self.enc_pool = nn.AvgPool1d(kernel_size=self.sampling_factor, stride=None)#, padding='valid')
        self.enc_act = nn.Identity()

        self.dec_upsample = nn.Upsample(scale_factor=self.sampling_factor)
        self.dec_tcn = TemporalConvolutionalNetwork(num_channels[-1][1], num_channels[-1][1], num_channels, kernel_size, dropout, predict_timestep=-1, output_linear=True)

    def forward(self, x):
        # Encode
        x_enc = self.enc_tcn(x).permute(0, 2, 1)
        x_enc = self.ecn_conv1d(x_enc)
        x_enc = self.enc_pool(x_enc)
        x_enc = self.enc_act(x_enc)

        x_dec = self.dec_upsample(x_enc).permute(0, 2, 1)
        x_dec = self.dec_tcn(x_dec)
        
        return x_dec

class TemporalConvolutionalMultiresAutoencoder(nn.Module):
    """
    Implementation of the TemporalConvolutionalAutoencoder (TCN-AE).
    Thill, Markus; Konen, Wolfgang; Bäck, Thomas (2020) Time Series Encodings with Temporal Convolutional Networks 
    http://www.gm.fh-koeln.de/ciopwebpub/Thill20a.d/bioma2020-tcn.pdf

    Parameters
    ----------
        num_inputs : int
            Number of input channels.

        num_outputs : int
            Number of output channels.

        num_channels : List[Tuple[int]]
            List of tuples (num_hidden, num_outputs) in each layer of TCN.

        kernel_size : int
            Kernel size of the convolutional layers.

        dropout : float
            Dropout probability.
    """
    def __init__(self, num_inputs : int, num_outputs : int, num_channels : List[Tuple[int, int]], kernel_size : int = 2, dropout : float = 0.2) -> None:
        super(TemporalConvolutionalMultiresAutoencoder, self).__init__()

        self.sampling_factor = 4

        # Encoder
        self.enc_tcn = TemporalConvolutionalMultiresNetwork(num_inputs, num_channels[-1][1], num_channels, kernel_size, len(num_channels), dropout, predict_timestep=-1, output_linear=True, residual=False, inv_dilation=False)
        self.ecn_conv1d = nn.Conv1d(num_channels[-1][1], num_channels[-1][1], kernel_size=1, padding='same')
        self.enc_pool = nn.AvgPool1d(kernel_size=self.sampling_factor, stride=None)#, padding='valid')
        self.enc_act = nn.Identity()

        self.dec_upsample = nn.Upsample(scale_factor=self.sampling_factor)
        self.dec_tcn = TemporalConvolutionalMultiresNetwork(num_channels[-1][1], num_outputs, num_channels, kernel_size, len(num_channels), dropout, predict_timestep=-1, output_linear=True, residual=False, inv_dilation=True)

    def forward(self, x):
        # Encode
        x_enc = self.enc_tcn(x).permute(0, 2, 1)
        x_enc = self.ecn_conv1d(x_enc)
        x_enc = self.enc_pool(x_enc)
        x_enc = self.enc_act(x_enc)

        x_dec = self.dec_upsample(x_enc).permute(0, 2, 1)
        x_dec = self.dec_tcn(x_dec)
        
        return x_dec

