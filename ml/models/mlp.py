import torch
from torch import nn

from typing import List

activation_functions = {'identity': nn.Identity, 'relu': nn.ReLU, 'tanh': nn.Tanh}
normalization_functions = {'none': None, 'batch_norm': nn.BatchNorm1d}

class MLPNet(nn.Module):
    """
    A simple MLP-based neural network for temperature correction prediction.

    Parameters
    ----------
        input_dim : int
            The number of input features.

        hidden_dim : int
            The number of features in the hidden dimensions.

        output_dim : int
            The number of output features. Defaults to 1.

        num_layers : int
            The number of linear layers in the network. Defaults to 4.

        normalization : str
            Normalization to be used. One of ['none', 'batchnorm']. Defaults to 'none', which means no normalization.

        activation : str
            Activation function, defaults to nn.Identity.

        dropout : float
            Dropout probability. Defaults to 0.25.
    """
    def __init__(self, input_dim : int, hidden_dim : int, output_dim : int = 1, num_layers : int = 4, normalization : str = 'none', activation : str = 'identity', dropout : float = 0.25) -> None:
        super(MLPNet, self).__init__()

        self.input_dim = input_dim
        self.hidden_dim = hidden_dim
        self.output_dim = output_dim
        self.dropout = dropout

        self.normalization = normalization_functions[normalization]
        self.activation = activation_functions[activation]
        
        # Input layer
        layers = [
            nn.Linear(input_dim, hidden_dim),
            self.activation()
        ]
        if self.normalization is not None:
            layers.append(self.normalization(hidden_dim))
        # Hidden layers
        for i in range(num_layers-2):
            layers.append(nn.Linear(hidden_dim, hidden_dim))
            layers.append(self.activation())
            if self.normalization is not None:
                layers.append(self.normalization(hidden_dim))
        # Output layer
        layers.append(nn.Dropout(self.dropout))
        layers.append(nn.Linear(hidden_dim, output_dim))
        
        self.net = nn.Sequential(*layers)

    def forward(self, x : torch.Tensor) -> torch.Tensor:
        """
        Parameters
        ----------
            x : torch.Tensor
                Tensor of input features to be fed to the network of shape (batch_size, input_dim).
        """
        return self.net(x)


class MLPFlexNet(nn.Module):
    """
    A simple MLP-based neural network for temperature correction prediction.

    Parameters
    ----------
        input_dim : int
            The number of input features.

        hidden_dim : List[int]
            The number of features in the hidden dimensions.

        output_dim : int
            The number of output features. Defaults to 1.

        normalization : str
            Normalization to be used. One of ['none', 'batchnorm']. Defaults to 'none', which means no normalization.

        activation : str
            Activation function, defaults to nn.Identity.

        dropout : float
            Dropout probability. Defaults to 0.25.
    """
    def __init__(self, input_dim : int, hidden_dim : List[int], output_dim : int = 1, normalization : str = 'none', activation : str = 'identity', dropout : float = 0.25) -> None:
        super(MLPFlexNet, self).__init__()

        self.input_dim = input_dim
        self.hidden_dim = hidden_dim
        self.output_dim = output_dim
        self.dropout = dropout

        self.normalization = normalization_functions[normalization]
        self.activation = activation_functions[activation]
        
        # Input layer
        layers = [
            nn.Linear(input_dim, hidden_dim[0]),
            self.activation()
        ]
        if self.normalization is not None:
            layers.append(self.normalization(hidden_dim[0]))
        # Hidden layers
        for i in range(1, len(hidden_dim)):
            layers.append(nn.Linear(hidden_dim[i-1], hidden_dim[i]))
            layers.append(self.activation())
            if self.normalization is not None:
                layers.append(self.normalization(hidden_dim[i]))
        # Output layer
        layers.append(nn.Dropout(self.dropout))
        layers.append(nn.Linear(hidden_dim[-1], output_dim))
        
        self.net = nn.Sequential(*layers)

    def forward(self, x : torch.Tensor) -> torch.Tensor:
        """
        Parameters
        ----------
            x : torch.Tensor
                Tensor of input features to be fed to the network of shape (batch_size, input_dim).
        """
        return self.net(x)
