import pandas as pd
import numpy as np
import datetime as dt


def get_sequence_indices(data: pd.DataFrame, window_size: int, max_dist: int = 48, index_filter: np.array = None) -> list:
    """
    Produce all the start and end index positions that are needed to produce the sub-sequences.

    Parameters
    ----------
        data : pd.DataFrame
            Sequence dataset from which to create sliced sub-sequences.

        window_size : int
            The desired length of each sub-sequence. Should be (input_sequence_length + target_sequence_length).
            E.g. if you want the model to consider past 100 time steps in order to predict the future 50 time
            steps, window_size = 100 + 50 = 150

        max_dist : int
            Maximum distance between start and end of the sequence in timesteps. Defaults to 48 (1 day).

        index_filter : np.array
            Array of indices to be considered as valid end sequence indices.

    Returns
    -------
        A list of tuples. Each tuple is (start_idx, end_idx) of a sub-sequence.
        The tuples should be used to slice the dataset into sub-sequences. These
        sub-sequences should then be passed into a function that slices them into input
        and target sequences.
    """

    index_data = data.copy()
    index_data['first_index'] = index_data.index - window_size
    index_data['first_index_measure_date'] = index_data['measure_date'].shift(window_size)
    index_data['first_index_station_id'] = index_data['station_id'].shift(window_size)
    timedelta = index_data['measure_date'] - index_data['first_index_measure_date']
    index_data['timesteps'] = timedelta.dt.components.days * 48 + timedelta.dt.components.hours * 2 + (timedelta.dt.components.minutes == 30).astype(int)
    index_data['index_filter_ok'] = (index_data.index - 1).isin(index_filter)
    index_data['station_ok'] = index_data['station_id'] == index_data['first_index_station_id']
    index_data['dist_ok'] = index_data['timesteps'] <= max_dist
    index_data = index_data[index_data['index_filter_ok'] & index_data['station_ok'] & index_data['dist_ok']]
    index_data = index_data.reset_index()

    return [tuple(r) for r in index_data[['first_index', 'index']].to_numpy()]