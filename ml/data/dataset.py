import os, sys
import pandas as pd
import numpy as np
import torch

from torch.utils.data import Dataset
from typing import Tuple, Callable, List, Optional
import math

from utils.data.dataframe import StationDataFrame, MultiStationDataFrame
from ml.data.misc import get_sequence_indices


class StationDataset(Dataset):
    """
    PyTorch Dataset class to handle data for handling data from IMIS stations.

    Parameters
    ----------    
        subsets : List[Tuple[str, str, bool]]
            List of tuples specifying the StationDataFrame instances this dataset should be composed of.

        input_feats : List[str]
            List of names of the columns to take from the dataframe as input features.

        target_feats : List[str]
            Name of the column to take as the target feature.

        sampling_filter : str
            Dataframe query filter that will be used to figure out which subset ot sample targets from. If empty string, all samples are considered. Defaults to ''.

        normalization : Tuple[str, str] 
            Type of normalization to be used on inputs and targets. First element of the tuple specifies type of inputs, the second for targets.
            One of ['none', 'meanstd', 'minmax']. Defaults to ['none', 'none'].

        norm_inputs : Tuple[List[float], List[float]]
            Normalization constants for the input variables according to the chosen normalization type. 
            If None, the normalization constants are computed automatically from the data. Defaults to None.

        norm_targets : Tuple[List[float], List[float]]
            Normalization constants for the target variable according to the chosen normalization type. 
            If None, the normalization constants are computed automatically from the data. Defaults to None.

        data_path : str
            Datapath to laod the data files from. Defaults to None.

        return_time_encoding : bool
            Whether to return time encoding. Defaults to False.

        return_timestamp : bool
            Whether to also return timeestamp information or not. Defaults to False.
    """

    def __init__(self, data_subsets : List[Tuple[str, str, bool]], input_feats : List[str], target_feats : List[str], sampling_filter : str = '', normalization : Tuple[str, str] = ('none', 'none'), norm_inputs : Tuple[List[float], List[float]] = None, norm_targets : Tuple[List[float], List[float]] = None, return_time_encoding : bool = False, return_timestamp : bool = False, data_path : str = None) -> None:
        super(StationDataset, self).__init__()

        self.data_subsets = data_subsets
        self.sampling_filter = sampling_filter
        self.input_feats = input_feats.copy()
        self.target_feats = target_feats
        self.normalization = normalization
        self.norm_inputs = norm_inputs
        self.data_path = data_path
        self.return_time_encoding = return_time_encoding
        self.return_timestamp = return_timestamp

        if self.norm_inputs  is not None:
            self.norm_inputs = (
                torch.tensor(norm_inputs[0], dtype=torch.float32),
                torch.tensor(norm_inputs[1], dtype=torch.float32)
            )
        self.norm_targets = norm_targets
        if self.norm_targets  is not None:
            self.norm_targets = (
                torch.tensor(norm_targets[0], dtype=torch.float32),
                torch.tensor(norm_targets[1], dtype=torch.float32)
            )

        # Read and filter data using a dataframe class                
        self.multidata_frame = self.create_multidata_frame(data_subsets)
        self.df = self.multidata_frame.get_data_frame()
        self.df['no_snow'] = self.df['no_snow'].fillna(0)

        # Get indices to be used for target sampling
        if self.sampling_filter != '':
            self.sample_idxs = self.df.query(self.sampling_filter).index.to_numpy()
        else:
            self.sample_idxs = self.df.index.to_numpy()

        # Compute normalization values 
        if self.norm_inputs is None:
            if self.normalization[0] == 'meanstd':
                self.norm_inputs = (
                    torch.tensor(self.df[self.input_feats].to_numpy().astype(np.float32).mean(axis=0), dtype=torch.float32),
                    torch.tensor(self.df[self.input_feats].to_numpy().astype(np.float32).std(axis=0), dtype=torch.float32)
                )
            elif self.normalization[0] == 'minmax':
                self.norm_inputs = (
                    torch.tensor(self.df[self.input_feats].to_numpy().astype(np.float32).min(axis=0), dtype=torch.float32),
                    torch.tensor(self.df[self.input_feats].to_numpy().astype(np.float32).max(axis=0), dtype=torch.float32)
                )
        if self.norm_targets is None:
            if self.normalization[1] == 'meanstd':
                self.norm_targets = (
                    torch.tensor(self.df[self.target_feats].to_numpy().astype(np.float32).mean(), dtype=torch.float32),
                    torch.tensor(self.df[self.target_feats].to_numpy().astype(np.float32).std(), dtype=torch.float32)
                )
            elif self.normalization == 'minmax':
                self.norm_targets[1] = (
                    torch.tensor(self.df[self.target_feats].to_numpy().astype(np.float32).min(), dtype=torch.float32),
                    torch.tensor(self.df[self.target_feats].to_numpy().astype(np.float32).max(), dtype=torch.float32)
                )

    def __len__(self) -> int:
        return len(self.sample_idxs)

    def create_multidata_frame(self, data_subsets : List[Tuple[str, str, bool]]) -> MultiStationDataFrame:
        dataframes = []
        for station_id, query, interp in data_subsets:
            if len(query) > 0:
                if self.data_path is None:
                    dataframes.append(StationDataFrame(station_id, lambda df : df.query(query), interp))
                else:
                    dataframes.append(StationDataFrame(station_id, lambda df : df.query(query), interp, None, None, False, self.data_path))
            else:
                if self.data_path is None:
                    dataframes.append(StationDataFrame(station_id, lambda df : df, interp))
                else:
                    dataframes.append(StationDataFrame(station_id, lambda df : df, interp, None, None, False, self.data_path))
                
        return MultiStationDataFrame(dataframes)

    def normalize_data(self, inputs : torch.Tensor, norm_type : str, norm_constants : Tuple[torch.Tensor, torch.Tensor]) -> torch.Tensor:
        if norm_type == 'meanstd':
            return (inputs - norm_constants[0]) / norm_constants[1]
        elif norm_type == 'minmax':
            return (inputs - norm_constants[0]) / (norm_constants[1] - norm_constants[0])
        elif norm_type == 'none':
            return inputs
        else:
            raise NotImplementedError("Unknown data normalization type requested.")

    def unnormalize_data(self, inputs : torch.Tensor, norm_type : str, norm_constants : Tuple[torch.Tensor, torch.Tensor]) -> torch.Tensor:
        if norm_type == 'meanstd':
            return inputs * norm_constants[1] + norm_constants[0]
        elif norm_type == 'minmax':
            return inputs * (norm_constants[1] - norm_constants[0]) + norm_constants[0]
        elif norm_type == 'none':
            return inputs
        else:
            raise NotImplementedError("Unknown data normalization type requested.")

    def __getitem__(self, index : int) -> Tuple[torch.Tensor]:
        """
        Convert a row of data from the Pandas dataframe into PyTorch tensor and return.
        
        Parameters
        ----------
            index : int
                Row (a datasample) of the dataframe to be processed.
        
        Returns
        -------
            (torch.Tensor, torch.Tensor) : Tuple of tensors where the first one represents input and second one the target.
        """

        # Reindex
        index = self.sample_idxs[index]

        inp_df = self.df.iloc[index][self.input_feats].to_numpy()        
        inp = torch.tensor(inp_df.astype(np.float32), dtype=torch.float32)
        inp = self.normalize_data(inp, self.normalization[0], self.norm_inputs)

        tgt = torch.tensor(self.df.iloc[index][self.target_feats], dtype=torch.float32)
        tgt = self.normalize_data(tgt, self.normalization[1], self.norm_targets)

        if self.return_timestamp:
            tstamp = torch.tensor(self.df.iloc[index:index+1]['measure_date'].values.astype(np.int64) // 10 ** 9)
            return inp, tgt, tstamp
        elif self.return_time_encoding:
            df_date = self.df[['measure_date']].iloc[index:index+1]
            df_date['month'] = df_date.measure_date.apply(lambda x : x.month)
            df_date['day'] = df_date.measure_date.apply(lambda x : x.day)
            df_date['weekday'] = df_date.measure_date.apply(lambda x : x.dayofweek, True)
            df_date['hour'] = df_date.measure_date.apply(lambda x : x.hour)
            df_date['minute'] = df_date.measure_date.apply(lambda x : x.minute / 15)
            tenc = torch.tensor(df_date.drop(['measure_date'], axis=1).values)
            return inp, tenc, tgt
        else:
            return inp, tgt


class StationSequenceDataset(StationDataset):
    """
    PyTorch Dataset class to handle data for handling data sequences from IMIS stations.

    Parameters
    ----------    
        subsets : List[Tuple[str, str, bool]]
            List of tuples specifying the StationDataFrame instances this dataset should be composed of.
            
        input_feats : List[str]
            List of names of the columns to take from the dataframe as input features.

        target_feats : List[str]
            Name of the column to take as the target feature.

        seq_len : int
            Desired length of each sequence.
            
        max_dist_timesteps : int
            Maximum distance between the start and the end of the sequence in timesteps.

        sampling_filter : str
            Dataframe query filter that will be used to figure out which subset ot sample targets from. If empty string, all samples are considered. Defaults to ''.

        normalization : Tuple[str, str] 
            Type of normalization to be used on inputs and targets. First element of the tuple specifies type of inputs, the second for targets.
            One of ['none', 'meanstd', 'minmax']. Defaults to ['none', 'none'].

        norm_inputs : Tuple[List[float], List[float]]
            Normalization constants for the input variables according to the chosen normalization type. 
            If None, the normalization constants are computed automatically from the data. Defaults to None.

        norm_targets : Tuple[List[float], List[float]]
            Normalization constants for the target variable according to the chosen normalization type. 
            If None, the normalization constants are computed automatically from the data. Defaults to None.

        return_time_encoding : bool
            Whether to return time encoding. Defaults to False.

        return_timestamp : bool
            Whether to also return timeestamp information or not. Defaults to False.

        data_path : str
            Datapath to laod the data files from. Defaults to None.
    """
    def __init__(self, data_subsets : List[Tuple[str, str, bool]], input_feats : List[str], target_feats : List[str], seq_len : int, max_dist_timesteps : int, sampling_filter : str = '', normalization : Tuple[str, str] = ('none', 'none'), norm_inputs : Tuple[List[float], List[float]] = None, norm_targets : Tuple[List[float], List[float]] = None, return_time_encoding : bool = False, return_timestamp : bool = False, data_path : str = None) -> None:
        super(StationSequenceDataset, self).__init__(data_subsets, input_feats, target_feats, sampling_filter, normalization, norm_inputs, norm_targets, return_time_encoding, return_timestamp, data_path)

        self.seq_len = seq_len
        self.max_dist_timesteps = max_dist_timesteps
        self.sequence_idxs = get_sequence_indices(self.df, window_size=seq_len, max_dist=max_dist_timesteps, index_filter=self.sample_idxs)

    def __len__(self) -> int:
        return len(self.sequence_idxs)

    def __getitem__(self, index : int) -> Tuple[torch.Tensor]:
        """
        Convert a row of data from the Pandas dataframe into PyTorch tensor and return.
        
        Parameters
        ----------
            index : int
                Row (a datasample) of the dataframe to be processed.
        
        Returns
        -------
            (torch.Tensor, torch.Tensor) : Tuple of tensors where the first one represents input and second one the target.
        """

        start_idx, end_idx = self.sequence_idxs[index]

        inp_df = self.df.iloc[start_idx:end_idx][self.input_feats].to_numpy()
        inp = torch.tensor(inp_df.astype(np.float32), dtype=torch.float32)
        inp = self.normalize_data(inp, self.normalization[0], self.norm_inputs)
        tgt = torch.tensor(self.df.iloc[start_idx:end_idx][self.target_feats].to_numpy().astype(np.float32), dtype=torch.float32)
        tgt = self.normalize_data(tgt, self.normalization[1], self.norm_targets)

        if self.return_timestamp:
            tstamp = torch.tensor(self.df.iloc[start_idx:end_idx]['measure_date'].values.astype(np.int64) // 10 ** 9)
        
        if self.return_time_encoding:
            df_date = self.df[['measure_date']].iloc[start_idx:end_idx]
            df_date['month'] = df_date.measure_date.apply(lambda x : x.month, True)
            df_date['day'] = df_date.measure_date.apply(lambda x : x.day, True)
            df_date['weekday'] = df_date.measure_date.apply(lambda x : x.dayofweek, True)
            df_date['hour'] = df_date.measure_date.apply(lambda x : x.hour, True)
            df_date['minute'] = df_date.measure_date.apply(lambda x : x.minute / 15, True)
            tenc = torch.tensor(df_date.drop(['measure_date'], axis=1).values)
        
        if self.return_timestamp and self.return_time_encoding:
            return inp, tenc, tgt, tstamp
        elif self.return_timestamp:
            return inp, tgt, tstamp
        elif self.return_time_encoding:
            return inp, tenc, tgt
        else:
            return inp, tgt


class StationLabeledSequenceDataset(StationDataset):
    """
    PyTorch Dataset class to handle data for handling data sequences from IMIS stations.

    Parameters
    ----------    
        subsets : List[Tuple[str, str, bool]]
            List of tuples specifying the StationDataFrame instances this dataset should be composed of.
            
        input_feats : List[str]
            List of names of the columns to take from the dataframe as input features.

        target_feats : List[str]
            Name of the column to take as the target feature.

        seq_len : int
            Desired length of each sequence.
            
        max_dist_timesteps : int
            Maximum distance between the start and the end of the sequence in timesteps.

        sampling_filter : str
            Dataframe query filter that will be used to figure out which subset ot sample targets from. If empty string, all samples are considered. Defaults to ''.

        normalization : Tuple[str, str] 
            Type of normalization to be used on inputs and targets. First element of the tuple specifies type of inputs, the second for targets.
            One of ['none', 'meanstd', 'minmax']. Defaults to ['none', 'none'].

        norm_inputs : Tuple[List[float], List[float]]
            Normalization constants for the input variables according to the chosen normalization type. 
            If None, the normalization constants are computed automatically from the data. Defaults to None.

        norm_targets : Tuple[List[float], List[float]]
            Normalization constants for the target variable according to the chosen normalization type. 
            If None, the normalization constants are computed automatically from the data. Defaults to None.

        data_path : str
            Datapath to laod the data files from. Defaults to None.
    """

    def __init__(self, data_subsets : List[Tuple[str, str, bool]], input_feats : List[str], target_feats : List[str], seq_len : int, max_dist_timesteps : int, sampling_filter : str = '', normalization : Tuple[str, str] = ('none', 'none'), norm_inputs : Tuple[List[float], List[float]] = None, norm_targets : Tuple[List[float], List[float]] = None, data_path : str = None) -> None:
        super(StationLabeledSequenceDataset, self).__init__(data_subsets, input_feats, target_feats, sampling_filter, normalization, norm_inputs, norm_targets, False, False, data_path)

        self.seq_len = seq_len
        self.max_dist_timesteps = max_dist_timesteps
        self.sequence_idxs = get_sequence_indices(self.df, window_size=seq_len, max_dist=max_dist_timesteps, index_filter=self.sample_idxs)

    def __len__(self) -> int:
        return len(self.sequence_idxs)

    def __getitem__(self, index : int) -> Tuple[torch.Tensor]:
        """
        Convert a row of data from the Pandas dataframe into PyTorch tensor and return.
        
        Parameters
        ----------
            index : int
                Row (a datasample) of the dataframe to be processed.
        
        Returns
        -------
            (torch.Tensor, torch.Tensor) : Tuple of tensors where the first one represents input and second one the target.
        """

        start_idx, end_idx = self.sequence_idxs[index]

        inp_df = self.df.iloc[start_idx:end_idx][self.input_feats].to_numpy()
        inp = torch.tensor(inp_df.astype(np.float32), dtype=torch.float32)
        inp = self.normalize_data(inp, self.normalization[0], self.norm_inputs)

        tgt = torch.tensor(self.df.iloc[start_idx:end_idx][self.target_feats].to_numpy().astype(np.float32), dtype=torch.float32)
        tgt = self.normalize_data(tgt, self.normalization[1], self.norm_targets)

        tgt_mean = torch.mean(tgt, dim=0)
        tgt_lbl = torch.gt(tgt_mean[0], 0.0)                                                 # TSS
        tgt_lbl = torch.logical_and(torch.lt(tgt_mean[1], 300.0), tgt_lbl).float()           # RSWR
        #tgt_lbl = torch.logical_and(torch.gt(tgt_mean[3], 0.025), tgt_lbl).float()          # HS
        
        return inp, tgt_lbl.unsqueeze(-1).repeat(inp.shape[0], 1)


